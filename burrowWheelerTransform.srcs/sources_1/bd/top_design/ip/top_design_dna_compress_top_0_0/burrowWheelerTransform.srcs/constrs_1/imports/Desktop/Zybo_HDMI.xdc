## This file is a general .xdc for the ZYBO Rev B board
## To use it in a project:
## - uncomment the lines corresponding to used pins
## - rename the used signals according to the project

set_property CLOCK_DEDICATED_ROUTE BACKBONE [get_nets -of_objects [get_ports sys_clock]]

##Clock signal
set_property -dict {PACKAGE_PIN L16 IOSTANDARD LVCMOS33} [get_ports sys_clock]
create_clock -period 8.000 -name sys_clk_pin -waveform {0.000 4.000} -add [get_ports sys_clock]


##Switches
set_property -dict {PACKAGE_PIN G15 IOSTANDARD LVCMOS33} [get_ports {sw[0]}]
set_property -dict {PACKAGE_PIN P15 IOSTANDARD LVCMOS33} [get_ports {sw[1]}]
set_property -dict {PACKAGE_PIN W13 IOSTANDARD LVCMOS33} [get_ports {sw[2]}]
set_property -dict {PACKAGE_PIN T16 IOSTANDARD LVCMOS33} [get_ports {sw[3]}]


##Buttons
set_property -dict { PACKAGE_PIN R18   IOSTANDARD LVCMOS33 } [get_ports { start }]; #IO_L20N_T3_34 Sch=BTN0
set_property -dict { PACKAGE_PIN P16   IOSTANDARD LVCMOS33 } [get_ports { btn[0] }]; #IO_L24N_T3_34 Sch=BTN1
set_property -dict { PACKAGE_PIN V16   IOSTANDARD LVCMOS33 } [get_ports { btn[1] }]; #IO_L18P_T2_34 Sch=BTN2
set_property -dict { PACKAGE_PIN Y16   IOSTANDARD LVCMOS33 } [get_ports { btn[2] }]; #IO_L7P_T1_34 Sch=BTN3


##LEDs
set_property -dict {PACKAGE_PIN M14 IOSTANDARD LVCMOS33} [get_ports {led[0]}]
set_property -dict {PACKAGE_PIN M15 IOSTANDARD LVCMOS33} [get_ports {led[1]}]
set_property -dict {PACKAGE_PIN G14 IOSTANDARD LVCMOS33} [get_ports {led[2]}]
set_property -dict {PACKAGE_PIN D18 IOSTANDARD LVCMOS33} [get_ports {led[3]}]


##I2S Audio Codec
#set_property -dict { PACKAGE_PIN K18   IOSTANDARD LVCMOS33 } [get_ports ac_bclk]; #IO_L12N_T1_MRCC_35 Sch=AC_BCLK
#set_property -dict { PACKAGE_PIN T19   IOSTANDARD LVCMOS33 } [get_ports ac_mclk]; #IO_25_34 Sch=AC_MCLK
#set_property -dict { PACKAGE_PIN P18   IOSTANDARD LVCMOS33 } [get_ports ac_muten]; #IO_L23N_T3_34 Sch=AC_MUTEN
#set_property -dict { PACKAGE_PIN M17   IOSTANDARD LVCMOS33 } [get_ports ac_pbdat]; #IO_L8P_T1_AD10P_35 Sch=AC_PBDAT
#set_property -dict { PACKAGE_PIN L17   IOSTANDARD LVCMOS33 } [get_ports ac_pblrc]; #IO_L11N_T1_SRCC_35 Sch=AC_PBLRC
#set_property -dict { PACKAGE_PIN K17   IOSTANDARD LVCMOS33 } [get_ports ac_recdat]; #IO_L12P_T1_MRCC_35 Sch=AC_RECDAT
#set_property -dict { PACKAGE_PIN M18   IOSTANDARD LVCMOS33 } [get_ports ac_reclrc]; #IO_L8N_T1_AD10N_35 Sch=AC_RECLRC


##Audio Codec/external EEPROM IIC bus
#set_property -dict { PACKAGE_PIN N18   IOSTANDARD LVCMOS33 } [get_ports ac_scl]; #IO_L13P_T2_MRCC_34 Sch=AC_SCL
#set_property -dict { PACKAGE_PIN N17   IOSTANDARD LVCMOS33 } [get_ports ac_sda]; #IO_L23P_T3_34 Sch=AC_SDA


##Additional Ethernet signals
#set_property -dict { PACKAGE_PIN F16   IOSTANDARD LVCMOS33 } [get_ports eth_int_b]; #IO_L6P_T0_35 Sch=ETH_INT_B
#set_property -dict { PACKAGE_PIN E17   IOSTANDARD LVCMOS33 } [get_ports eth_rst_b]; #IO_L3P_T0_DQS_AD1P_35 Sch=ETH_RST_B


##HDMI Signals
#set_property -dict {PACKAGE_PIN H17 IOSTANDARD TMDS_33} [get_ports hdmi_in_clk_n]
#set_property -dict {PACKAGE_PIN H16 IOSTANDARD TMDS_33} [get_ports hdmi_in_clk_p]
#set_property IOSTANDARD TMDS_33 [get_ports {hdmi_in_data_n[0]}]
#set_property PACKAGE_PIN D20 [get_ports {hdmi_in_data_n[0]}]
#set_property PACKAGE_PIN D19 [get_ports {hdmi_in_data_p[0]}]
#set_property IOSTANDARD TMDS_33 [get_ports {hdmi_in_data_p[0]}]
#set_property IOSTANDARD TMDS_33 [get_ports {hdmi_in_data_n[1]}]
#set_property PACKAGE_PIN B20 [get_ports {hdmi_in_data_n[1]}]
#set_property PACKAGE_PIN C20 [get_ports {hdmi_in_data_p[1]}]
#set_property IOSTANDARD TMDS_33 [get_ports {hdmi_in_data_p[1]}]
#set_property IOSTANDARD TMDS_33 [get_ports {hdmi_in_data_n[2]}]
#set_property PACKAGE_PIN A20 [get_ports {hdmi_in_data_n[2]}]
#set_property PACKAGE_PIN B19 [get_ports {hdmi_in_data_p[2]}]
#set_property IOSTANDARD TMDS_33 [get_ports {hdmi_in_data_p[2]}]
#set_property -dict { PACKAGE_PIN E19   IOSTANDARD LVCMOS33 } [get_ports hdmi_cec]; #IO_L5N_T0_AD9N_35 Sch=HDMI_CEC
#set_property PACKAGE_PIN E18 [get_ports {hdmi_hpd[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {hdmi_hpd[0]}]
#set_property -dict {PACKAGE_PIN F17 IOSTANDARD LVCMOS33} [get_ports hdmi_out_en]
#set_property -dict {PACKAGE_PIN G17 IOSTANDARD LVCMOS33} [get_ports hdmi_in_ddc_scl_io]
#set_property -dict {PACKAGE_PIN G18 IOSTANDARD LVCMOS33} [get_ports hdmi_in_ddc_sda_io]

#create_clock -period 9.259 -name hdmi_in_clk_p -waveform {0.000 4.629} [get_ports hdmi_in_clk_p]

##Pmod Header JA (XADC)
#set_property -dict { PACKAGE_PIN N15   IOSTANDARD LVCMOS33 } [get_ports { ja_p[0] }]; #IO_L21P_T3_DQS_AD14P_35 Sch=JA1_R_p
#set_property -dict { PACKAGE_PIN L14   IOSTANDARD LVCMOS33 } [get_ports { ja_p[1] }]; #IO_L22P_T3_AD7P_35 Sch=JA2_R_P
#set_property -dict { PACKAGE_PIN K16   IOSTANDARD LVCMOS33 } [get_ports { ja_p[2] }]; #IO_L24P_T3_AD15P_35 Sch=JA3_R_P
#set_property -dict { PACKAGE_PIN K14   IOSTANDARD LVCMOS33 } [get_ports { ja_p[3] }]; #IO_L20P_T3_AD6P_35 Sch=JA4_R_P
#set_property -dict { PACKAGE_PIN N16   IOSTANDARD LVCMOS33 } [get_ports { ja_n[0] }]; #IO_L21N_T3_DQS_AD14N_35 Sch=JA1_R_N
#set_property -dict { PACKAGE_PIN L15   IOSTANDARD LVCMOS33 } [get_ports { ja_n[1] }]; #IO_L22N_T3_AD7N_35 Sch=JA2_R_N
#set_property -dict { PACKAGE_PIN J16   IOSTANDARD LVCMOS33 } [get_ports { ja_n[2] }]; #IO_L24N_T3_AD15N_35 Sch=JA3_R_N
#set_property -dict { PACKAGE_PIN J14   IOSTANDARD LVCMOS33 } [get_ports { ja_n[3] }]; #IO_L20N_T3_AD6N_35 Sch=JA4_R_N


##Pmod Header JB
#set_property -dict { PACKAGE_PIN T20   IOSTANDARD LVCMOS33 } [get_ports { jb_p[0] }]; #IO_L15P_T2_DQS_34 Sch=JB1_p
#set_property -dict { PACKAGE_PIN U20   IOSTANDARD LVCMOS33 } [get_ports { jb_n[0] }]; #IO_L15N_T2_DQS_34 Sch=JB1_N
#set_property -dict { PACKAGE_PIN V20   IOSTANDARD LVCMOS33 } [get_ports { jb_p[1] }]; #IO_L16P_T2_34 Sch=JB2_P
#set_property -dict { PACKAGE_PIN W20   IOSTANDARD LVCMOS33 } [get_ports { jb_n[1] }]; #IO_L16N_T2_34 Sch=JB2_N
#set_property -dict { PACKAGE_PIN Y18   IOSTANDARD LVCMOS33 } [get_ports { jb_p[2] }]; #IO_L17P_T2_34 Sch=JB3_P
#set_property -dict { PACKAGE_PIN Y19   IOSTANDARD LVCMOS33 } [get_ports { jb_n[2] }]; #IO_L17N_T2_34 Sch=JB3_N
#set_property -dict { PACKAGE_PIN W18   IOSTANDARD LVCMOS33 } [get_ports { jb_p[3] }]; #IO_L22P_T3_34 Sch=JB4_P
#set_property -dict { PACKAGE_PIN W19   IOSTANDARD LVCMOS33 } [get_ports { jb_n[3] }]; #IO_L22N_T3_34 Sch=JB4_N


##Pmod Header JC
#set_property -dict { PACKAGE_PIN V15   IOSTANDARD LVCMOS33 } [get_ports { jc_p[0] }]; #IO_L10P_T1_34 Sch=JC1_P
#set_property -dict { PACKAGE_PIN W15   IOSTANDARD LVCMOS33 } [get_ports { jc_n[0] }]; #IO_L10N_T1_34 Sch=JC1_N
#set_property -dict { PACKAGE_PIN T11   IOSTANDARD LVCMOS33 } [get_ports { jc_p[1] }]; #IO_L1P_T0_34 Sch=JC2_P
#set_property -dict { PACKAGE_PIN T10   IOSTANDARD LVCMOS33 } [get_ports { jc_n[1] }]; #IO_L1N_T0_34 Sch=JC2_N
#set_property -dict { PACKAGE_PIN W14   IOSTANDARD LVCMOS33 } [get_ports { jc_p[2] }]; #IO_L8P_T1_34 Sch=JC3_P
#set_property -dict { PACKAGE_PIN Y14   IOSTANDARD LVCMOS33 } [get_ports { jc_n[2] }]; #IO_L8N_T1_34 Sch=JC3_N
#set_property -dict { PACKAGE_PIN T12   IOSTANDARD LVCMOS33 } [get_ports { jc_p[3] }]; #IO_L2P_T0_34 Sch=JC4_P
#set_property -dict { PACKAGE_PIN U12   IOSTANDARD LVCMOS33 } [get_ports { jc_n[3] }]; #IO_L2N_T0_34 Sch=JC4_N


##Pmod Header JD
#set_property -dict { PACKAGE_PIN T14   IOSTANDARD LVCMOS33 } [get_ports { jd_p[0] }]; #IO_L5P_T0_34 Sch=JD1_P
#set_property -dict { PACKAGE_PIN T15   IOSTANDARD LVCMOS33 } [get_ports { jd_n[0] }]; #IO_L5N_T0_34 Sch=JD1_N
#set_property -dict { PACKAGE_PIN P14   IOSTANDARD LVCMOS33 } [get_ports { jd_p[1] }]; #IO_L6P_T0_34 Sch=JD2_P
#set_property -dict { PACKAGE_PIN R14   IOSTANDARD LVCMOS33 } [get_ports { jd_n[1] }]; #IO_L6N_T0_VREF_34 Sch=JD2_N
#set_property -dict { PACKAGE_PIN U14   IOSTANDARD LVCMOS33 } [get_ports { jd_p[2] }]; #IO_L11P_T1_SRCC_34 Sch=JD3_P
#set_property -dict { PACKAGE_PIN U15   IOSTANDARD LVCMOS33 } [get_ports { jd_n[2] }]; #IO_L11N_T1_SRCC_34 Sch=JD3_N
#set_property -dict { PACKAGE_PIN V17   IOSTANDARD LVCMOS33 } [get_ports { jd_p[3] }]; #IO_L21P_T3_DQS_34 Sch=JD4_P
#set_property -dict { PACKAGE_PIN V18   IOSTANDARD LVCMOS33 } [get_ports { jd_n[3] }]; #IO_L21N_T3_DQS_34 Sch=JD4_N


##Pmod Header JE
#set_property -dict { PACKAGE_PIN V12   IOSTANDARD LVCMOS33 } [get_ports { je[0] }]; #IO_L4P_T0_34 Sch=JE1
#set_property -dict { PACKAGE_PIN W16   IOSTANDARD LVCMOS33 } [get_ports { je[1] }]; #IO_L18N_T2_34 Sch=JE2
#set_property -dict { PACKAGE_PIN J15   IOSTANDARD LVCMOS33 } [get_ports { je[2] }]; #IO_25_35 Sch=JE3
#set_property -dict { PACKAGE_PIN H15   IOSTANDARD LVCMOS33 } [get_ports { je[3] }]; #IO_L19P_T3_35 Sch=JE4
#set_property -dict { PACKAGE_PIN V13   IOSTANDARD LVCMOS33 } [get_ports { je[4] }]; #IO_L3N_T0_DQS_34 Sch=JE7
#set_property -dict { PACKAGE_PIN U17   IOSTANDARD LVCMOS33 } [get_ports { je[5] }]; #IO_L9N_T1_DQS_34 Sch=JE8
#set_property -dict { PACKAGE_PIN T17   IOSTANDARD LVCMOS33 } [get_ports { je[6] }]; #IO_L20P_T3_34 Sch=JE9
#set_property -dict { PACKAGE_PIN Y17   IOSTANDARD LVCMOS33 } [get_ports { je[7] }]; #IO_L7N_T1_34 Sch=JE10


##USB-OTG overcurrent detect pin
#set_property -dict { PACKAGE_PIN U13   IOSTANDARD LVCMOS33 } [get_ports otg_oc]; #IO_L3P_T0_DQS_PUDC_B_34 Sch=OTG_OC


##VGA Connector
#set_property PACKAGE_PIN M19 [get_ports {vga_pRed[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pRed[0]}]
#set_property PACKAGE_PIN L20 [get_ports {vga_pRed[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pRed[1]}]
#set_property PACKAGE_PIN J20 [get_ports {vga_pRed[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pRed[2]}]
#set_property PACKAGE_PIN G20 [get_ports {vga_pRed[3]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pRed[3]}]
#set_property PACKAGE_PIN F19 [get_ports {vga_pRed[4]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pRed[4]}]
#set_property PACKAGE_PIN H18 [get_ports {vga_pGreen[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pGreen[0]}]
#set_property PACKAGE_PIN N20 [get_ports {vga_pGreen[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pGreen[1]}]
#set_property PACKAGE_PIN L19 [get_ports {vga_pGreen[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pGreen[2]}]
#set_property PACKAGE_PIN J19 [get_ports {vga_pGreen[3]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pGreen[3]}]
#set_property PACKAGE_PIN H20 [get_ports {vga_pGreen[4]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pGreen[4]}]
#set_property PACKAGE_PIN F20 [get_ports {vga_pGreen[5]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pGreen[5]}]
#set_property PACKAGE_PIN P20 [get_ports {vga_pBlue[0]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pBlue[0]}]
#set_property PACKAGE_PIN M20 [get_ports {vga_pBlue[1]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pBlue[1]}]
#set_property PACKAGE_PIN K19 [get_ports {vga_pBlue[2]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pBlue[2]}]
#set_property PACKAGE_PIN J18 [get_ports {vga_pBlue[3]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pBlue[3]}]
#set_property PACKAGE_PIN G19 [get_ports {vga_pBlue[4]}]
#set_property IOSTANDARD LVCMOS33 [get_ports {vga_pBlue[4]}]
#set_property -dict {PACKAGE_PIN P19 IOSTANDARD LVCMOS33} [get_ports vga_pHSync]
#set_property -dict {PACKAGE_PIN R19 IOSTANDARD LVCMOS33} [get_ports vga_pVSync]



#create_debug_core u_ila_0 ila
#set_property ALL_PROBE_SAME_MU true [get_debug_cores u_ila_0]
#set_property ALL_PROBE_SAME_MU_CNT 4 [get_debug_cores u_ila_0]
#set_property C_ADV_TRIGGER true [get_debug_cores u_ila_0]
#set_property C_DATA_DEPTH 1024 [get_debug_cores u_ila_0]
#set_property C_EN_STRG_QUAL true [get_debug_cores u_ila_0]
#set_property C_INPUT_PIPE_STAGES 0 [get_debug_cores u_ila_0]
#set_property C_TRIGIN_EN false [get_debug_cores u_ila_0]
#set_property C_TRIGOUT_EN false [get_debug_cores u_ila_0]
#set_property port_width 1 [get_debug_ports u_ila_0/clk]
#connect_debug_port u_ila_0/clk [get_nets [list hdmi_vga_design_i/processing_system7_0/inst/FCLK_CLK0]]
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe0]
#set_property port_width 32 [get_debug_ports u_ila_0/probe0]
#connect_debug_port u_ila_0/probe0 [get_nets [list {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[0]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[1]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[2]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[3]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[4]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[5]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[6]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[7]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[8]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[9]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[10]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[11]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[12]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[13]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[14]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[15]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[16]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[17]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[18]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[19]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[20]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[21]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[22]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[23]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[24]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[25]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[26]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[27]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[28]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[29]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[30]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARADDR[31]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe1]
#set_property port_width 32 [get_debug_ports u_ila_0/probe1]
#connect_debug_port u_ila_0/probe1 [get_nets [list {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[0]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[1]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[2]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[3]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[4]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[5]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[6]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[7]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[8]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[9]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[10]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[11]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[12]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[13]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[14]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[15]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[16]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[17]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[18]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[19]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[20]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[21]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[22]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[23]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[24]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[25]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[26]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[27]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[28]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[29]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[30]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWADDR[31]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe2]
#set_property port_width 2 [get_debug_ports u_ila_0/probe2]
#connect_debug_port u_ila_0/probe2 [get_nets [list {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_BRESP[0]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_BRESP[1]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe3]
#set_property port_width 32 [get_debug_ports u_ila_0/probe3]
#connect_debug_port u_ila_0/probe3 [get_nets [list {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[0]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[1]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[2]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[3]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[4]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[5]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[6]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[7]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[8]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[9]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[10]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[11]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[12]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[13]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[14]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[15]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[16]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[17]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[18]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[19]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[20]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[21]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[22]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[23]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[24]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[25]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[26]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[27]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[28]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[29]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[30]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RDATA[31]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe4]
#set_property port_width 2 [get_debug_ports u_ila_0/probe4]
#connect_debug_port u_ila_0/probe4 [get_nets [list {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RRESP[0]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RRESP[1]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe5]
#set_property port_width 32 [get_debug_ports u_ila_0/probe5]
#connect_debug_port u_ila_0/probe5 [get_nets [list {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[0]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[1]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[2]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[3]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[4]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[5]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[6]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[7]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[8]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[9]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[10]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[11]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[12]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[13]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[14]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[15]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[16]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[17]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[18]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[19]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[20]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[21]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[22]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[23]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[24]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[25]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[26]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[27]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[28]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[29]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[30]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WDATA[31]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe6]
#set_property port_width 4 [get_debug_ports u_ila_0/probe6]
#connect_debug_port u_ila_0/probe6 [get_nets [list {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WSTRB[0]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WSTRB[1]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WSTRB[2]} {hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WSTRB[3]}]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe7]
#set_property port_width 1 [get_debug_ports u_ila_0/probe7]
#connect_debug_port u_ila_0/probe7 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARREADY]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe8]
#set_property port_width 1 [get_debug_ports u_ila_0/probe8]
#connect_debug_port u_ila_0/probe8 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_ARVALID]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe9]
#set_property port_width 1 [get_debug_ports u_ila_0/probe9]
#connect_debug_port u_ila_0/probe9 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWREADY]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe10]
#set_property port_width 1 [get_debug_ports u_ila_0/probe10]
#connect_debug_port u_ila_0/probe10 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_AWVALID]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe11]
#set_property port_width 1 [get_debug_ports u_ila_0/probe11]
#connect_debug_port u_ila_0/probe11 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_BREADY]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe12]
#set_property port_width 1 [get_debug_ports u_ila_0/probe12]
#connect_debug_port u_ila_0/probe12 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_BVALID]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe13]
#set_property port_width 1 [get_debug_ports u_ila_0/probe13]
#connect_debug_port u_ila_0/probe13 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RREADY]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe14]
#set_property port_width 1 [get_debug_ports u_ila_0/probe14]
#connect_debug_port u_ila_0/probe14 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_RVALID]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe15]
#set_property port_width 1 [get_debug_ports u_ila_0/probe15]
#connect_debug_port u_ila_0/probe15 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WREADY]]
#create_debug_port u_ila_0 probe
#set_property PROBE_TYPE DATA_AND_TRIGGER [get_debug_ports u_ila_0/probe16]
#set_property port_width 1 [get_debug_ports u_ila_0/probe16]
#connect_debug_port u_ila_0/probe16 [get_nets [list hdmi_vga_design_i/ps7_0_axi_periph_M00_AXI_WVALID]]
#set_property C_CLK_INPUT_FREQ_HZ 300000000 [get_debug_cores dbg_hub]
#set_property C_ENABLE_CLK_DIVIDER false [get_debug_cores dbg_hub]
#set_property C_USER_SCAN_CHAIN 1 [get_debug_cores dbg_hub]
#connect_debug_port dbg_hub/clk [get_nets clk]
