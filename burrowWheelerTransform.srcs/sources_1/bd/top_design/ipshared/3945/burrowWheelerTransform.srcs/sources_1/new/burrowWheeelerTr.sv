`timescale 1ns / 1ps
`include "dna_representation.vh"
`include "bram_interface.vh"


module burrowWheeelerTr
#(parameter SYMBOL_QTY = 20,
  parameter BANK_NO = 4,
  parameter DNA_ADDR_WIDTH = 12,
  parameter IDS_ADDR_WIDTH = 12,
  parameter MEM_DELAY = 3)
(
    input wire clock,
    input wire reset,

    bram_ids_if ids_left_if,
    bram_ids_if ids_right_if,

    /* mem preloading inputs */
    bram_DNA_if DNA_mem_in_if,
    input wire DNA_written,
    output wire [DNA_ADDR_WIDTH-1:0] DNA_mem_addr_if,
    output wire ids_left_valid_out,
    output wire ids_right_valid_out,
    output reg [IDS_ADDR_WIDTH-1:0] debug_fb[SYMBOL_QTY+1]
);
    localparam PRELOAD = 3'b001;
    localparam COUNTER_READ = 3'b010;
    localparam SORTER_READ = 3'b100;

    localparam SYMBOL_PER_BANK = SYMBOL_QTY/BANK_NO;

    reg [2:0] DNA_mem_mutex = PRELOAD;

    bram_DNA_if DNA_mem_sorter_if();

    reg qty_ready;
    reg count_genes = 0, count_genes_clr = 0, count_genes_clr_nxt = 0;;


    reg [DNA_ADDR_WIDTH-1:0] DNA_mem_counter_addr, DNA_mem_addr_nxt;
    reg DNA_mem_write_prev, DNA_mem_write_prev_buf;;
    reg new_dna_in_mem, gen_ctr_last;

    assign DNA_mem_addr_if = (PRELOAD == DNA_mem_mutex)      ? DNA_mem_in_if.addr    :
                             (COUNTER_READ == DNA_mem_mutex) ? DNA_mem_counter_addr  : 
                                                               DNA_mem_sorter_if.addr;
    assign new_dna_in_mem = DNA_mem_write_prev && !DNA_mem_in_if.we;
    assign DNA_mem_sorter_if.out = DNA_mem_in_if.out;
    assign DNA_mem_sorter_if.we  = DNA_mem_in_if.we;
    assign DNA_mem_sorter_if.valid = DNA_mem_in_if.valid;
    assign DNA_mem_sorter_if.ready = DNA_mem_in_if.ready;
    assign DNA_mem_sorter_if.out_addr = DNA_mem_in_if.out_addr;


    assign DNA_mem_in_if.re = (COUNTER_READ == DNA_mem_mutex) ? count_genes : DNA_mem_sorter_if.re;

    always @(posedge clock)
    begin
      if(reset) begin
        gen_ctr_last <= 'b0;
        DNA_mem_write_prev <= 'b0;
        count_genes_clr <= 'b0;
      end else begin
        count_genes_clr <= count_genes_clr_nxt;
        DNA_mem_write_prev <= DNA_mem_write_prev_buf;
  
        /* sets last_char when genome_counter.sv sets last addr of DNA_mem,
          for sorter.sv to know that it can begin addressing the memory */
        if(DNA_mem_counter_addr == SYMBOL_QTY-SYMBOL_PER_BANK-1 && DNA_mem_mutex == COUNTER_READ)
          gen_ctr_last <= 'b1;
        else
          gen_ctr_last <= 'b0;
        end
    end

    always @*
    begin
      if(new_dna_in_mem)
        count_genes = 'b1;
      else if(count_genes_clr) begin
        count_genes = 'b0;
        count_genes_clr_nxt = 'b0;
      end

      if(SYMBOL_QTY-1 == DNA_mem_counter_addr)
        count_genes_clr_nxt = 'b1;


      if(DNA_mem_in_if.we)
        DNA_mem_mutex = PRELOAD;
      else if(count_genes)
        DNA_mem_mutex = COUNTER_READ;
      else
        DNA_mem_mutex = SORTER_READ;

      DNA_mem_write_prev_buf = DNA_mem_in_if.we;
    end
    


    reg [(IDS_ADDR_WIDTH-1):0] qty_out [4], genes_ctr;
    reg new_dna_in_mem_delayed;

    delayLine
     #( .WIDTH(1),
        .DELAY_CYCLES(MEM_DELAY+2))
     delay_for_gen_ctr(
        .clk(clock),
        .rst(reset),
        .din(new_dna_in_mem),
        .dout(new_dna_in_mem_delayed)
    );

    genome_counter2
     #( .SYMBOL_QTY(SYMBOL_QTY),
        .IDS_ADDR_WIDTH(IDS_ADDR_WIDTH))
    gen_ctr2(
        .DNA_in(DNA_mem_in_if.out),
        .C_pos(qty_out[0]),
        .G_pos(qty_out[1]),
        .T_pos(qty_out[2]),
        .X_pos(qty_out[3]),
        .all_genes_qty(genes_ctr),
        .valid_out(qty_ready),
        .first_char(new_dna_in_mem_delayed),
        .clock(clock),
        .reset(reset)
    );

    sorter2
    #( .SYMBOL_QTY(SYMBOL_QTY),
       .IDS_ADDR_WIDTH(IDS_ADDR_WIDTH))
    ids_sorter(
        .C_pos(qty_out[0]),
        .G_pos(qty_out[1]),
        .T_pos(qty_out[2]),
        
        .DNA_mem_access(SORTER_READ == DNA_mem_mutex),
        .DNA_if(DNA_mem_sorter_if),
        .ids_left_if(ids_left_if),
        .ids_right_if(ids_right_if),

        .qty_ready(qty_ready),
        .last_char(gen_ctr_last),
        .ids_left_valid_out(ids_left_valid_out),
        .ids_right_valid_out(ids_right_valid_out),

        .debug_fb(debug_fb),

        .clock(clock),
        .reset(reset)
    );

    counter_by_bank
    #(.MAX_COUNT(SYMBOL_QTY),
      .SYM_PER_BANK(SYMBOL_QTY/BANK_NO),
      .WIDTH(DNA_ADDR_WIDTH),
      .BANK_NO(BANK_NO))
    bank_aware_ctr (
      .count(DNA_mem_counter_addr),
      .reset((DNA_written | reset) || (DNA_mem_mutex != COUNTER_READ)),
      .clock(clock)
    );
    
endmodule

