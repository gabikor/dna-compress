`ifndef DNA_REPRESENTATION_VH
`define DNA_REPRESENTATION_VH
    parameter [1:0] A = 2'b00;
    parameter [1:0] C = 2'b01;
    parameter [1:0] G = 2'b10;
    parameter [1:0] T = 2'b11;

    // function void print_char(input reg [1:0] char);
    // begin
    //     case(char)
    //     A: begin
    //        $write("A,");
    //        end
    //     C: begin 
    //        $write("C,");
    //        end
    //     G: begin 
    //        $write("G,");      
    //        end
    //     T: begin    
    //        $write("T,");         
    //        end
    //     endcase  
    // end
    // endfunction

`endif

