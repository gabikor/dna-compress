`timescale 1ns / 1ps
`include "dna_representation.vh"
`include "bram_interface.vh"


module dna_compress
#(parameter SYMBOL_QTY = 20,
  parameter DNA_ADDR_WIDTH = 12,
  parameter IDS_ADDR_WIDTH = 12,
  parameter MEM_DELAY = 3,
  parameter BANK_NO = 4)
(
    input wire clock,
    input wire reset,

    bram_DNA_if DNA_mem_in_if,
    input wire DNA_written,
    output reg [IDS_ADDR_WIDTH-1:0] debug_fb[SYMBOL_QTY+1],
    output wire bwt_ready,
    output reg [13:0] deb_dna_mem_addr
);
    
    bram_ids_if ids_left_if();
    bram_ids_if ids_right_if();
    bram_DNA_if DNA_mem_arb_if();

    wire [DNA_ADDR_WIDTH-1:0] DNA_mem_addr_if;
    wire ids_left_valid_out;
    wire ids_right_valid_out;

    assign bwt_ready = ids_left_valid_out || ids_right_valid_out;
    assign deb_dna_mem_addr = /*{DNA_mem_arb_if.in,*/DNA_mem_arb_if.addr;

    /* parse interface replacing address for mutex'ed one */
    assign DNA_mem_arb_if.addr = DNA_mem_addr_if;
    assign DNA_mem_arb_if.in = DNA_mem_in_if.in;
    assign DNA_mem_arb_if.we = DNA_mem_in_if.we;
    assign DNA_mem_arb_if.re = DNA_mem_in_if.re;
    assign DNA_mem_in_if.out = DNA_mem_arb_if.out;
    assign DNA_mem_in_if.out_addr = DNA_mem_arb_if.out_addr;
    assign DNA_mem_in_if.valid = DNA_mem_arb_if.valid;
    assign DNA_mem_in_if.ready = DNA_mem_arb_if.ready;

burrowWheeelerTr #( 
    .SYMBOL_QTY      (SYMBOL_QTY),
    .IDS_ADDR_WIDTH  (IDS_ADDR_WIDTH),
    .DNA_ADDR_WIDTH  (DNA_ADDR_WIDTH),
    .BANK_NO         (BANK_NO),
    .MEM_DELAY       (MEM_DELAY))
bw(
    .clock(clock),
    .reset(reset),

    .ids_left_if(ids_left_if),
    .ids_right_if(ids_right_if),

    .DNA_mem_in_if(DNA_mem_in_if),
    .DNA_written(DNA_written),
    .DNA_mem_addr_if(DNA_mem_addr_if),

    .ids_left_valid_out(ids_left_valid_out),
    .ids_right_valid_out(ids_right_valid_out),
    .debug_fb(debug_fb)
);

ext_mem_arbiter #(
  .ART_DELAY(MEM_DELAY),
  .MEMORY_WIDTH(DNA_ADDR_WIDTH),
  .MAX_SYMBOL_NO(SYMBOL_QTY),
  .BANK_NO(BANK_NO))
DNA_mem_arbiter (
  .clock(clock),
  .reset(reset),
  .main_mem_if( DNA_mem_arb_if)
);

ext_mem_arbiter_ids #(
  .ART_DELAY(MEM_DELAY),
  .MEMORY_WIDTH(IDS_ADDR_WIDTH),
  .MAX_SYMBOL_NO(SYMBOL_QTY),
  .BANK_NO(BANK_NO))
ids_left_mem_arbiter (
  .clock(clock),
  .reset(reset),
  .main_mem_if(ids_left_if)
);

ext_mem_arbiter_ids #(
  .ART_DELAY(MEM_DELAY),
  .MEMORY_WIDTH(IDS_ADDR_WIDTH),
  .MAX_SYMBOL_NO(SYMBOL_QTY),
  .BANK_NO(BANK_NO))
ids_right_mem_arbiter (
  .clock(clock),
  .reset(reset),
  .main_mem_if(ids_right_if)
);
      
endmodule
