`timescale 1ns / 1ps

module counter_by_bank
#( parameter MAX_COUNT,
   parameter SYM_PER_BANK,
   parameter WIDTH,
   parameter BANK_NO)
(
    output reg [WIDTH-1:0] count,
    input reset,
    input clock
);

   reg [WIDTH-1:0] ctr_nxt, ctr_buf;
   reg [2:0] bank_no, bank_no_nxt = 0;

    always @(posedge clock)
    begin
        if (reset) begin
            ctr_buf <= 0;
            bank_no <= 'b0;
        end else begin
            ctr_buf <= ctr_nxt;
            bank_no <= bank_no_nxt;
        end
    end


    always @* 
    begin
        bank_no_nxt = bank_no +1;
        if(bank_no == BANK_NO-1) begin
          bank_no_nxt = 'b0;
          ctr_nxt = ctr_buf+1;
        end else begin
          ctr_nxt = ctr_buf;
        end
    end

    assign count = ctr_buf + SYM_PER_BANK*bank_no;



endmodule
