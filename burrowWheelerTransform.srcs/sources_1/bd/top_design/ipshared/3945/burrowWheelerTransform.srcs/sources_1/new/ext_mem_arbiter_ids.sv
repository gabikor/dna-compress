`timescale 1ns / 1ps

module ext_mem_arbiter_ids
#(  parameter ART_DELAY = 3,
	parameter BANK_NO = 4,
	parameter MAX_SYMBOL_NO = 20,
	parameter MEMORY_WIDTH = 11)
(
	input wire clock,
	input wire reset,
	bram_ids_if main_mem_if
);

localparam DATA_WIDTH = 32;
localparam SYMBOL_PER_BANK = (MAX_SYMBOL_NO/BANK_NO);

bram_ids_if mem_if[BANK_NO]();
reg [MEMORY_WIDTH-1:0] mem_if_addr_next[BANK_NO];
reg [DATA_WIDTH-1:0] mem_if_in_next[BANK_NO];
reg mem_if_we_next[BANK_NO], mem_if_re_next[BANK_NO];
reg [1:0] sel_buf[ART_DELAY], sel_buf_nxt[ART_DELAY];
reg [1:0] bank_sel_in, bank_sel_out;
reg ready, ready_nxt;

  delayLine
  #(.WIDTH(2),
    .DELAY_CYCLES(ART_DELAY+1))
  delay_bank_sel(
    .clk(clock),
    .rst(reset),
    .din(bank_sel_in),
    .dout(bank_sel_out)
  );

assign main_mem_if.out = (0 == bank_sel_out) ? mem_if[0].out :
                         (1 == bank_sel_out) ? mem_if[1].out :
                         (2 == bank_sel_out) ? mem_if[2].out :
                                               mem_if[3].out;

assign main_mem_if.valid = (0 == bank_sel_out) ? mem_if[0].valid :
                           (1 == bank_sel_out) ? mem_if[1].valid :
                           (2 == bank_sel_out) ? mem_if[2].valid :
                                                 mem_if[3].valid;

assign main_mem_if.out_addr = (0 == bank_sel_out) ? mem_if[0].out_addr :
                              (1 == bank_sel_out) ? mem_if[1].out_addr :
                              (2 == bank_sel_out) ? mem_if[2].out_addr :
                                                    mem_if[3].out_addr;

assign main_mem_if.ready = (0 == bank_sel_in) ? mem_if[0].ready :
                           (1 == bank_sel_in) ? mem_if[1].ready :
                           (2 == bank_sel_in) ? mem_if[2].ready :
                                                mem_if[3].ready;

always_ff @(posedge clock or posedge reset) begin
	if(reset) begin

		mem_if[0].addr <= 'b0;
		mem_if[1].addr <= 'b0;
		mem_if[2].addr <= 'b0;
		mem_if[3].addr <= 'b0;
		mem_if[0].we   <= 'b0;
        mem_if[1].we   <= 'b0;
        mem_if[2].we   <= 'b0;
        mem_if[3].we   <= 'b0;
        mem_if[0].in   <= 'b0;
        mem_if[1].in   <= 'b0;
        mem_if[2].in   <= 'b0;
        mem_if[3].in   <= 'b0;
        mem_if[0].re   <= 'b0;
        mem_if[1].re   <= 'b0;
        mem_if[2].re   <= 'b0;
        mem_if[3].re   <= 'b0;

	end else begin

		mem_if[0].addr <= mem_if_addr_next[0];
		mem_if[1].addr <= mem_if_addr_next[1];
		mem_if[2].addr <= mem_if_addr_next[2];
		mem_if[3].addr <= mem_if_addr_next[3];
		mem_if[0].we   <= mem_if_we_next[0];
        mem_if[1].we   <= mem_if_we_next[1];
        mem_if[2].we   <= mem_if_we_next[2];
        mem_if[3].we   <= mem_if_we_next[3];
        mem_if[0].in   <= mem_if_in_next[0];
        mem_if[1].in   <= mem_if_in_next[1];
        mem_if[2].in   <= mem_if_in_next[2];
        mem_if[3].in   <= mem_if_in_next[3];
        mem_if[0].re   <= mem_if_re_next[0];
        mem_if[1].re   <= mem_if_re_next[1];
        mem_if[2].re   <= mem_if_re_next[2];
        mem_if[3].re   <= mem_if_re_next[3];

	end
end

always_comb 
begin

	mem_if_addr_next[0] = mem_if[0].addr;
	mem_if_addr_next[1] = mem_if[1].addr;
	mem_if_addr_next[2] = mem_if[2].addr;
	mem_if_addr_next[3] = mem_if[3].addr;

    mem_if_in_next[0]   = mem_if[0].in;
    mem_if_in_next[1]   = mem_if[1].in;
    mem_if_in_next[2]   = mem_if[2].in;
    mem_if_in_next[3]   = mem_if[3].in;

    mem_if_we_next[0]   = 'b0;
    mem_if_we_next[1]   = 'b0;
    mem_if_we_next[2]   = 'b0;
    mem_if_we_next[3]   = 'b0;

    mem_if_re_next[0]   = 'b0;
    mem_if_re_next[1]   = 'b0;
    mem_if_re_next[2]   = 'b0;
    mem_if_re_next[3]   = 'b0;

	bank_sel_in = main_mem_if.addr/SYMBOL_PER_BANK; //zał: addr_space % bank_NO = 0
	mem_if_addr_next[bank_sel_in] = main_mem_if.addr;
	mem_if_re_next[bank_sel_in] = main_mem_if.re;
	mem_if_we_next[bank_sel_in] = main_mem_if.we;
	mem_if_in_next[bank_sel_in] = main_mem_if.in;

end


ext_mem_pretender_ids
#(.ART_DELAY(ART_DELAY),
  .MEMORY_WIDTH(MEMORY_WIDTH),
  .DATA_WIDTH(DATA_WIDTH))
ext_mem_bank1 (
	.clock(clock),
	.reset(reset),
	.mem_if(mem_if[0])
);

ext_mem_pretender_ids
#(.ART_DELAY(ART_DELAY),
  .MEMORY_WIDTH(MEMORY_WIDTH),
  .DATA_WIDTH(DATA_WIDTH))
ext_mem_bank2 (
	.clock(clock),
	.reset(reset),
	.mem_if(mem_if[1])
);

ext_mem_pretender_ids
#(.ART_DELAY(ART_DELAY),
  .MEMORY_WIDTH(MEMORY_WIDTH),
  .DATA_WIDTH(DATA_WIDTH))
ext_mem_bank3 (
	.clock(clock),
	.reset(reset),
	.mem_if(mem_if[2])
);

ext_mem_pretender_ids
#(.ART_DELAY(ART_DELAY),
  .MEMORY_WIDTH(MEMORY_WIDTH),
  .DATA_WIDTH(DATA_WIDTH))
ext_mem_bank4 (
	.clock(clock),
	.reset(reset),
	.mem_if(mem_if[3])
);


endmodule

