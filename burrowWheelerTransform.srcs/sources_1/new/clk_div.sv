`timescale 1ns / 1ps

module clk_div(
    input clk_in,
    output clk_out,
    input reset
    );

reg [7:0] delay, delay_nxt; // div by 512

always @ (posedge clk_in)
begin
	if(reset)
		delay <= 'b0;
	else 
		delay <= delay_nxt;
end

always @ *
begin
    delay_nxt = delay +1;
end

assign clk_out = delay[7];

endmodule
