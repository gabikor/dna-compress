`timescale 1ns / 1ps
`include "dna_representation.vh"

module genome_counter
#(parameter SYMBOL_QTY = 10,
  parameter SYMBOL_IDX_WIDTH = 4)
(
    input wire [1:0] DNA_in[SYMBOL_QTY],
    output reg [1:0] DNA_out[SYMBOL_QTY],
    output reg [(SYMBOL_IDX_WIDTH-1):0] A_quantity,
    output reg [(SYMBOL_IDX_WIDTH-1):0] C_quantity,
    output reg [(SYMBOL_IDX_WIDTH-1):0] G_quantity,
    /* no need to count T's */
    input wire valid_in,
    output reg valid_out,
    input wire clock,
    input wire reset
);
 
    reg [SYMBOL_IDX_WIDTH-1:0] A_qty, C_qty, G_qty;
        
    always @(posedge clock)
    begin
        if(reset) begin
            DNA_out <= '{default:0};
            A_quantity <= 'b0;
            C_quantity <= 'b0;
            G_quantity <= 'b0;
            valid_out  <= 'b0;
        end else begin
            A_quantity <= A_qty;
            C_quantity <= C_qty;
            G_quantity <= G_qty;
            DNA_out <= DNA_in;
            valid_out <= valid_in;
        end
            
    end
    
    reg [1:0] temp_symbol;

    always @*
    begin
        A_qty = 0;
        C_qty = 0;
        G_qty = 0;
        for(int symbol = 0;symbol <SYMBOL_QTY;symbol +=1)
        begin
            temp_symbol = DNA_in[symbol];
            A_qty += (A == temp_symbol);
            C_qty += (C == temp_symbol);
            G_qty += (G == temp_symbol);
        end
    end
    
endmodule


