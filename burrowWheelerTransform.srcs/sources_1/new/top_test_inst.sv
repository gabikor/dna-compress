`timescale 1ns / 1ps

`include "dna_representation.vh"
`include "bram_interface.vh"

module top_test_inst(
	input [2:0] btn,
	input sys_clock,
	input start,
	input [3:0] sw,
	output reg [3:0] led
    );

localparam SYMBOL_QTY = 40;
localparam IDS_ADDR_WIDTH = 12;
localparam DNA_ADDR_WIDTH = 12;
localparam MEM_DELAY = 3;
localparam BANK_NO = 4;
localparam SYMBOL_PER_BANK = SYMBOL_QTY/BANK_NO;

reg [1:0] bank_no_nxt='b0, bank_no='b0;
reg [3:0] sym_no_nxt='b0, sym_no='b0;
reg [3:0] o_debug_fb;

reg preloaded = 0;
reg preloaded_nxt = 0;
reg pre_ld = 0;
reg pre_ld_nxt = 1;


assign led[0] = preloaded;
assign led[2] = pre_ld_nxt;
assign led[3] = pre_ld;

reg [13:0] deb_dna_mem_addr;

always @(posedge sys_clock)
begin
	if(!start) begin
		sym_no <= 'b0;
		bank_no <= 'b0;
		preloaded <= 'b0;
		pre_ld <= 'b1;
	end else begin
		sym_no <= sym_no_nxt;
		bank_no <= bank_no_nxt;
		preloaded <= preloaded_nxt;
		pre_ld <= pre_ld_nxt;
	end
end

always @*
begin

	bank_no_nxt = bank_no;
	sym_no_nxt = sym_no;
	preloaded_nxt = preloaded;

	if(pre_ld) begin
		pre_ld_nxt = 'b1;
		bank_no_nxt = bank_no +1;
		if(bank_no == BANK_NO-1) begin
			sym_no_nxt = sym_no+1;
			if(SYMBOL_PER_BANK-1 == sym_no) begin
				sym_no_nxt = 'b0;
				pre_ld_nxt = 'b0;
				preloaded_nxt = 'b1;
			end
			bank_no_nxt = 'b0;
		end else begin
			sym_no_nxt = sym_no;
		end
	end

	case (sw[3:0])
		4'b0001: o_debug_fb = sym_no;
		4'b0010: o_debug_fb = pre_ld;
	
		default : o_debug_fb = 'b0;
	endcase
end


endmodule
