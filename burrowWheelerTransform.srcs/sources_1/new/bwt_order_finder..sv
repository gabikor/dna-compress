`timescale 1ns / 1ps
`include "dna_representation.vh"

module bwt_order_finder
#( parameter SYMBOL_QTY = 10,
   parameter SYMBOL_IDX_WIDTH = 4)
(
   input wire [1:0] DNA_in[SYMBOL_QTY],
   input wire [SYMBOL_IDX_WIDTH-1:0] ids[SYMBOL_QTY],
   output reg [1:0] DNA_out[SYMBOL_QTY],
   output reg [1:0] DNA_bwt[SYMBOL_QTY],
   
   input wire valid_in,
   output reg valid_out,
   input wire clock,
   input wire reset
);

    localparam WIDTH = SYMBOL_QTY*2;

    function automatic [1:0] get_char(reg [SYMBOL_IDX_WIDTH-1:0] id);
    begin
        return(DNA_in[id]);
    end
    endfunction

    reg [1:0] DNA_bwt_nxt[SYMBOL_QTY];

    // function print_char(input reg [1:0] char);
    // begin
    //     case(char)
    //     A: begin
    //        $write("A,");
    //        end
    //     C: begin 
    //        $write("C,");
    //        end
    //     G: begin 
    //        $write("G,");      
    //        end
    //     T: begin    
    //        $write("T,");         
    //        end                             
    //     endcase  
    // end
    // endfunction

    always @(posedge clock)
    begin
        if(reset) begin
            DNA_out <= '{default:0};
            valid_out <= 'b0;
        end else begin
            DNA_out <= DNA_in;
            DNA_bwt <= DNA_bwt_nxt;
            valid_out <= valid_in;
        end
    end
  //  reg [1:0] char; // debug
  //  int id_idx;     // debug

    always @(ids)
    begin
        $display(""); // newline
        for(int i = 0; i < SYMBOL_QTY; i+=1)
        begin
            $write("%d,",ids[i]); 
        end
        $display(""); // newline
        for(int i = 0; i < SYMBOL_QTY; i+=1)
        begin
            DNA_bwt_nxt[i] = (ids[i] > 0) ? get_char(ids[i]-1) : get_char(SYMBOL_QTY-1);
          //  print_char(DNA_bwt[i]); 
    		//$display(""); // newline
    		// for (int j =0; j < SYMBOL_QTY; j+=1)
    		// begin
    		// 	id_idx = (ids[i]+j)%SYMBOL_QTY;
    		// 	char = get_char(id_idx);
    		// 	$write("%0d  ",id_idx);
    		// 	print_char(char);        
    		// end
    	end
    end

endmodule
