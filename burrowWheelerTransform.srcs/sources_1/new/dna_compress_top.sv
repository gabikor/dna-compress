`timescale 1ns / 1ps

`include "dna_representation.vh"
`include "bram_interface.vh"

module dna_compress_top(
	input [2:0] btn,
	input clock,
	input start,
	input reset,
	input [3:0] sw,
	output reg [3:0] led,
	output reg [31:0] o_debug_fb, //[SYMBOL_QTY], 
	output reg bwt_ready
    );

reg clk_divided, clk_divided_n1;



localparam SYMBOL_QTY = 40;
localparam IDS_ADDR_WIDTH = 12;
localparam DNA_ADDR_WIDTH = 12;
localparam MEM_DELAY = 3;
localparam BANK_NO = 4;
localparam SYMBOL_PER_BANK = SYMBOL_QTY/BANK_NO;

reg [1:0] bank_no_nxt='b0, bank_no='b0;
reg [3:0] sym_no_nxt='b0, sym_no='b0;
//wire clock;

reg [1:0] test_dna_10_0 [SYMBOL_QTY] = {A,C,T,G,T,A,G,C,C,T,C,G,A,T,T,A,C,A,C,T,C,G,A,T,T,A,C,A,C,T,A,G,C,C,T,T,G,T,A,G};
//const reg [1:0] test_dna_10_1 [SYMBOL_QTY] = {C,G,A,T,T,A,C,A,C,T,A,G,C,C,T,T,G,T,A,G,A,C,T,G,T,A,G,C,C,T,C,G,A,T,T,A,C,A,C,T};

reg [IDS_ADDR_WIDTH-1:0] result_test_10_0 [SYMBOL_QTY] = {25, 15, 27, 17, 0, 38, 5, 30, 22, 12, 26, 16, 7, 32, 20, 10, 28, 18, 8, 1, 33, 39, 21, 11, 6, 31, 36, 3, 24, 14, 37, 4, 29, 19, 9, 35, 2, 23, 13, 34};
reg [IDS_ADDR_WIDTH-1:0] debug_fb[SYMBOL_QTY];

bram_DNA_if preload_if();
bram_DNA_if preload_if_nxt();

reg preloaded = 0;
reg preloaded_nxt = 0;
reg bwt_ok = 0;
reg bwt_ok_nxt = 0;
//reg but3 = 0;
reg pre_ld = 0;
reg pre_ld_nxt = 1;
reg reset_n1 = 0;
//reg pre_reset = 'b1;
//reg reset_prev = 'b0;

reg [3:0] bad_bwt_ctr = 0;
reg [3:0] bad_bwt_ctr_nxt = 0;


assign led[0] = preloaded;
assign led[1] = bwt_ok;
assign led[2] = pre_ld_nxt;
assign led[3] = pre_ld;

reg [13:0] deb_dna_mem_addr;


always @(posedge clk_divided)
begin
	if(!start) begin
		sym_no <= 'b0;
		bank_no <= 'b0;
		preloaded <= 'b0;
		bwt_ok <= 'b0;
		pre_ld <= 'b1;
		bad_bwt_ctr <= 'b0;
	end else begin
		sym_no <= sym_no_nxt;
		bank_no <= bank_no_nxt;
		preloaded <= preloaded_nxt;
		bwt_ok <= bwt_ok_nxt;
		pre_ld <= pre_ld_nxt;
		bad_bwt_ctr <= bad_bwt_ctr_nxt;
	end
	//reset_prev <= reset;
end

always @*
begin
//	clk_check_next = clk_check +1;
//	if(reset_prev && !reset) pre_reset = 'b0;
	
	
	// preloaded_nxt = 'b0;
	// bank_no_nxt = 'b0;
	// sym_no_nxt = 'b0;

	if(bwt_ready) begin
		bwt_ok_nxt = 'b1;
		bad_bwt_ctr_nxt = 0;
		for(int i = 0 ; i < SYMBOL_QTY; i++) begin
			bwt_ok_nxt = bwt_ok_nxt & (debug_fb[i] == result_test_10_0[i]);
			bad_bwt_ctr_nxt = bad_bwt_ctr_nxt + (debug_fb[i] != result_test_10_0[i]);
			$write("%0d, ",debug_fb[i]);
		end
	end else begin
		bwt_ok_nxt = bwt_ok;
		bad_bwt_ctr_nxt = bad_bwt_ctr;
	end

	bank_no_nxt = bank_no;
	sym_no_nxt = sym_no;
	preloaded_nxt = preloaded;
    preload_if_nxt.we = !preloaded;

	if(pre_ld) begin
		pre_ld_nxt = 'b1;
		bank_no_nxt = bank_no +1;
		if(bank_no == BANK_NO-1) begin
			sym_no_nxt = sym_no+1;
			if(SYMBOL_PER_BANK-1 == sym_no) begin
				sym_no_nxt = 'b0;
				pre_ld_nxt = 'b0;
				preloaded_nxt = 'b1;
			end
			bank_no_nxt = 'b0;
		end else begin
			sym_no_nxt = sym_no;
		end
		preload_if_nxt.addr = sym_no+(SYMBOL_PER_BANK*bank_no);
	    preload_if_nxt.in = test_dna_10_0[sym_no+(SYMBOL_PER_BANK*bank_no)];
	    preload_if_nxt.we = 'b1;
	    preload_if_nxt.re = 'b0;
		//preload_DNA_char(sym_no+(SYMBOL_PER_BANK*bank_no),test_dna_10_0[sym_no+(SYMBOL_PER_BANK*bank_no)]);
	end

	case (sw[3:0])
		4'b0000: o_debug_fb = preload_if_nxt.addr;
		4'b0001: o_debug_fb = sym_no;
		4'b0010: o_debug_fb = pre_ld;
		//4'b0100: o_debug_fb = debug_fb[4];
		4'b1000: o_debug_fb = deb_dna_mem_addr;
		//4'b1111: o_debug_fb = clk_check;
	
		default : o_debug_fb = 0;// result_test_10_0[btn];
	endcase
end

reg [10:0] delay =0, delay_nxt=0; // div by 1024

always @ (posedge clock)
begin
	delay <= delay_nxt;
	reset_n1 <= !start;
	clk_divided_n1 <= clk_divided;
	preload_if.in <= preload_if_nxt.in;
	preload_if.addr <= preload_if_nxt.addr;
	preload_if.we <= preload_if_nxt.we;
	preload_if.re <= preload_if_nxt.re;
end

always @ *
begin
    delay_nxt = delay +1;
end

assign clk_divided = delay[10];


// task preload_DNA_char(logic [DNA_ADDR_WIDTH:0] addr, logic [1:0] data);
// begin
//     preload_if.addr = addr;
//     preload_if.in = data;
//     preload_if.we = 'b1;
//     preload_if.re = 'b0;
// end
// endtask

dna_compress #( 
    .SYMBOL_QTY(SYMBOL_QTY),
    .IDS_ADDR_WIDTH(IDS_ADDR_WIDTH),
    .MEM_DELAY(MEM_DELAY),
    .BANK_NO(BANK_NO))
dna_compress0(
    .clock(clk_divided),
    //.reset(pre_reset),
    .reset(reset_n1),
    .DNA_mem_in_if(preload_if),
    .DNA_written(preloaded),
    .bwt_ready(bwt_ready),
    //.debug_fb(debug_fb),
    .debug_fb(),
    .deb_dna_mem_addr(deb_dna_mem_addr)
);

endmodule
