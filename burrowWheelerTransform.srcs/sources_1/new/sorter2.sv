`timescale 1ns / 1ps
`include "dna_representation.vh"
`include "crc32.vh"

`define DEBUG

module sorter2
#( parameter SYMBOL_QTY = 10,
   parameter IDS_ADDR_WIDTH = 12)
(
   input wire [IDS_ADDR_WIDTH-1:0] C_pos,
   input wire [IDS_ADDR_WIDTH-1:0] G_pos,
   input wire [IDS_ADDR_WIDTH-1:0] T_pos,

   input wire DNA_mem_access,
   bram_DNA_if DNA_if,
   bram_ids_if ids_left_if,
   bram_ids_if ids_right_if,

   input wire qty_ready,
   input wire last_char,
   output reg ids_left_valid_out,
   output reg ids_right_valid_out,

   output reg [IDS_ADDR_WIDTH-1:0] debug_fb[SYMBOL_QTY+1],

   input wire clock,
   input wire reset
);

  localparam IDS_IDLE      = 3'b000; /* wait for previous modules to preload data and count genes */
  localparam IDS_GENERATE  = 3'b001; /* generate ids from in order last, 0, 1, 2 ... (last-1) */
  localparam IDS_L2R       = 3'b010; /* left to right */
  localparam IDS_R2L       = 3'b011; /* right to left */
  localparam IDS_FINISHED  = 3'b100; /* indicate sorting has been done */
  localparam PRE_IDS_L2R       = 3'b110; /* left to right */
  localparam PRE_IDS_R2L       = 3'b111; /* right to left */

  localparam DIR_L2R = 0;
  localparam DIR_R2L = 1;

  /* IDS GENERATE DATA */ 
  reg [IDS_ADDR_WIDTH-1:0] ids_generate_addr,   /* ids addr when generating addr (at first step of algorithm - when no data in ids_left nor ids right) */
                           ids_addr_sequential, /* ids addr when reading sequentially */
                           ids_addr_right_calc, /* ids addr when calculating addres to write new id (left 2 right) */
                           ids_addr_left_calc;  /* ids addr when calculating addres to write new id (right 2 left) */
  
  reg writing_dna_fifo = 0;
  reg dna_fifo_write_started = 0;

  reg [IDS_ADDR_WIDTH-1:0] iteration_ctr;
  reg [IDS_ADDR_WIDTH-1:0] pos_ctr[4],
                           pos_ctr_nxt[4],
                           all_pos_ctr,
                           all_pos_ctr_nxt,
                           written_symbols_ctr,
                           written_symbols_ctr_nxt;

	reg [IDS_ADDR_WIDTH-1:0] ids_right_in_nxt, //todo param same as in memory
             ids_right_addr_nxt,
             ids_left_in_nxt,
             ids_left_addr_nxt,
             ids_left_out_addr_prev,
             ids_right_out_addr_prev,
             ids_right_addr_prev,
             ids_left_addr_prev;

  reg [31:0] ids_right_crc32,
             ids_right_crc32_nxt,
             ids_left_crc32,
             ids_left_crc32_nxt,
             ids_comp_crc32,
             ids_comp_crc32_nxt;


  reg ids_right_we_nxt, 
      ids_left_we_nxt,
      ids_left_valid_nxt,
      ids_right_valid_nxt,
      ids_right_we_prev,
      ids_left_we_prev,
      ids_right_re_nxt,
      ids_left_re_nxt,
      ids_left_valid_prev,
      ids_right_valid_prev;

  reg [1:0] char_prev;
	reg [2:0] ids_source,ids_source_nxt = IDS_IDLE;

	reg state_change, state_change_nxt;

  reg [1:0] delay_state, delay_state_nxt;

  reg [IDS_ADDR_WIDTH-1:0] debug [SYMBOL_QTY];

  reg dna_read_fifo_empty, 
      dna_read_fifo_full,
      dna_read_fifo_valid, 
      dna_read_fifo_re,
      dna_fifo_re_nxt, 
      dna_fifo_valid_n1,
      dna_read_fifo_rst,
      dna_read_fifo_rst_nxt;

  reg [IDS_ADDR_WIDTH+1:0] dna_read_fifo_dout; /* plus 2 bits [dna_char + addr] */
  reg [IDS_ADDR_WIDTH-1:0] dna_prev_addr;
  reg dna_valid_prev;

  read_fifo DNA_read_fifo (
    .clk(clock),      // input wire clk
    .rst(reset/* || dna_read_fifo_rst*/),      // input wire rst
    .din({DNA_if.out,DNA_if.out_addr[IDS_ADDR_WIDTH-1:0]}),      // input wire [18 : 0] din
    .wr_en(DNA_if.valid && writing_dna_fifo),  // input wire wr_en
    .rd_en(dna_read_fifo_re),  // input wire rd_en
    .dout(dna_read_fifo_dout),    // output wire [14 : 0] dout
    .full(dna_read_fifo_full),    // output wire full
    .empty(dna_read_fifo_empty),  // output wire empty
    .valid(dna_read_fifo_valid)  // output wire valid
  );

  reg ids_read_fifo_empty, ids_read_fifo_empty_prev, ids_read_fifo_full,ids_read_fifo_valid, ids_read_fifo_we,ids_read_fifo_re, ids_read_fifo_re_nxt;
  reg fifo_rst_n1, fifo_rst_n2;


  reg [63:0] ids_read_fifo_dout, ids_read_fifo_din;

  reg [IDS_ADDR_WIDTH-1:0] ids_right_out_buf,
                           ids_left_out_buf;

  reg [IDS_ADDR_WIDTH-1:0] dna_if_out_addr_prev; /* dna addr space equals ids addr space (regarding reg size) */

  fifo_ids ids_read_fifo (
    .clk(clock),                   // input wire clk
    .rst(reset /*|| dna_read_fifo_rst*/),   // input wire rst //todo zapewnić WREN i RDEN 0 przez 2 cykle od zwolnienia resetu!
    .din(ids_read_fifo_din),      // input wire [63 : 0] din
    .wr_en(ids_read_fifo_we),     // input wire wr_en
    .rd_en(ids_read_fifo_re),     // input wire rd_en
    .dout(ids_read_fifo_dout),    // output wire [63 : 0] dout
    .full(ids_read_fifo_full),    // output wire full
    .empty(ids_read_fifo_empty),  // output wire empty
    .valid(ids_read_fifo_valid)   // output wire valid
  );

  delayLine
  #(.WIDTH(1),
    .DELAY_CYCLES(1))
  delay_fifo_valid_2_mem_we(
    .clk(clock),
    .rst(reset),
    .din(dna_read_fifo_valid),
    .dout(dna_fifo_valid_n1)
  );

  mem_sequential_read
  #(.WIDTH(16),
    .SYMBOL_QTY(SYMBOL_QTY))
  dna_seq_read (
    .clk(clock),
    .rst(reset || last_char),
    .start_val(0),
    .ready(DNA_if.ready),
    .addr(ids_generate_addr)
  );

  reg ids_seq_read_ready;
  reg ids_seq_read_rst;

  mem_sequential_read
  #(.WIDTH(16),
    .SYMBOL_QTY(SYMBOL_QTY))
  ids_seq_read (
    .clk(clock),
    .rst(reset || ids_seq_read_rst),
    .start_val(0),
    .ready(ids_seq_read_ready),
    .addr(ids_addr_sequential)
  );


  counter
  #(.MAX_COUNT(SYMBOL_QTY+1),
    .WIDTH(IDS_ADDR_WIDTH),
    .START_VAL(1)) /* count last char as first iteration */ 
  iteration_counter(
    .en('b1),
    .clock(state_change || last_char),
    .reset(last_char),
    .count(iteration_ctr)
  );

  /* never write to DNA_mem */
  assign DNA_if.we = 'b0;
  /* DNA_mem addr will be ids */
  assign DNA_if.addr = (IDS_GENERATE == ids_source) ? ids_generate_addr
                     : (IDS_R2L == ids_source)      ? get_prev_symbol(ids_right_out_buf,1) :
                                                      get_prev_symbol(ids_left_out_buf,1);

  assign ids_right_if.addr = (IDS_R2L == ids_source) ? ids_addr_sequential : ids_addr_right_calc;
  assign ids_left_if.addr = (IDS_L2R == ids_source) ? ids_addr_sequential : ids_addr_left_calc;


  assign DNA_if.re = (IDS_GENERATE == ids_source) ? 'b1 : ids_fifi2dna_mem_re;
 
	always_ff @(posedge clock)
    begin
    	if(reset) begin
        ids_source <= IDS_IDLE;
        pos_ctr[A] <= 'b0;
        pos_ctr[C] <= 'b0;
        pos_ctr[G] <= 'b0;
        pos_ctr[T] <= 'b0;
        all_pos_ctr  <= 'b1;
        state_change <= 'b0;
        ids_left_if.we  <= 'b0;
        ids_left_if.re  <= 'b0;
        ids_right_if.we <= 'b0;
        ids_right_if.re <= 'b0;
        ids_left_crc32  <= '{default:1};
        ids_right_crc32 <= '{default:1};
        ids_left_valid_out  <= 'b0;
        ids_right_valid_out <= 'b0;
        dna_prev_addr <= 'b0;
        dna_read_fifo_re <= 'b0;
        written_symbols_ctr <= 'b0;
        ids_read_fifo_re <= 'b0;
        dna_valid_prev <= 'b0;
        ids_right_out_addr_prev <= 'b0;
        ids_left_out_addr_prev <= 'b0;
        ids_right_valid_prev <= 'b0;
        ids_left_valid_prev <= 'b0;
        ids_right_addr_prev <= 'b0;
        ids_left_addr_prev <= 'b0;
        delay_state <= 'b0;

      end else begin

        dna_prev_addr <= dna_read_fifo_dout[IDS_ADDR_WIDTH-1:0];
        char_prev <= dna_read_fifo_dout[IDS_ADDR_WIDTH+1:IDS_ADDR_WIDTH];

        delay_state <= delay_state_nxt;

        ids_right_if.in     <= ids_right_in_nxt;
        ids_addr_right_calc <= ids_right_addr_nxt;
        ids_right_if.we     <= ids_right_we_nxt;
        ids_right_valid_out <= ids_right_valid_nxt;
        ids_right_crc32     <= ids_right_crc32_nxt;
        ids_right_we_prev   <= ids_right_if.we;
        ids_right_if.re     <= ids_right_re_nxt;
        ids_right_out_addr_prev <= ids_right_if.out_addr;
        ids_right_valid_prev <= ids_right_if.valid;

        ids_left_if.in      <= ids_left_in_nxt;
        ids_addr_left_calc  <= ids_left_addr_nxt;
        ids_left_if.we      <= ids_left_we_nxt;
        ids_left_valid_out  <= ids_left_valid_nxt;
        ids_left_crc32      <= ids_left_crc32_nxt;
        ids_left_we_prev    <= ids_left_if.we;
        ids_left_if.re      <= ids_left_re_nxt;
        ids_left_out_addr_prev <= ids_left_if.out_addr;
        ids_left_valid_prev <= ids_left_if.valid;
        
        pos_ctr[A]  <= pos_ctr_nxt[A];
        pos_ctr[C]  <= pos_ctr_nxt[C];
        pos_ctr[G]  <= pos_ctr_nxt[G];
        pos_ctr[T]  <= pos_ctr_nxt[T];
        all_pos_ctr <= all_pos_ctr_nxt;

        ids_source           <= ids_source_nxt;
        state_change         <= state_change_nxt;
        ids_comp_crc32       <= ids_comp_crc32_nxt;
        dna_read_fifo_re     <= dna_fifo_re_nxt;
        written_symbols_ctr  <= written_symbols_ctr_nxt;
        ids_read_fifo_re     <= ids_read_fifo_re_nxt;
        dna_if_out_addr_prev <= DNA_if.out_addr;
        dna_valid_prev       <= DNA_if.valid;
        dna_read_fifo_rst    <= dna_read_fifo_rst_nxt;

        ids_right_addr_prev <= ids_right_if.addr;
        ids_left_addr_prev <= ids_left_if.addr;

      end
    end

    reg [IDS_ADDR_WIDTH-1:0] next_id;

    always_comb
    begin

      pre_calc_cleanup();

      case(ids_source)

        IDS_IDLE: begin
          idle();
        end

        IDS_GENERATE: begin

          ids_source_nxt = IDS_GENERATE;

          gen2fifo_dna();

          mem_write_when_ready(dna_read_fifo_valid,
                              ids_right_if.ready,
                              dna_read_fifo_dout[IDS_ADDR_WIDTH+1:IDS_ADDR_WIDTH],
                              dna_fifo_re_nxt);
          mem_write_init(qty_ready);

          if(ids_right_if.we && (!ids_right_if.ready)) begin
            ids_right_we_nxt = 'b1;
          end else begin
            ids_right_we_nxt = dna_fifo_valid_n1;
          end
          written_symbols_ctr_nxt = dna_fifo_valid_n1 ? written_symbols_ctr +1 : written_symbols_ctr;

          /* In alg positions incrementations are after indexing. Here are before,
             so decremented value of pos related to previous char is taken */
          next_id = pos_ctr[char_prev]-1;
          ids_right_addr_nxt = next_id;
          ids_right_in_nxt = dna_prev_addr[IDS_ADDR_WIDTH-1:0];

          // if(ids_right_we_nxt)
          //   $display("GEN addr %0d   id %d",ids_right_addr_nxt, ids_right_in_nxt);

          if(SYMBOL_QTY == written_symbols_ctr) begin
            ids_source_nxt = PRE_IDS_R2L;
            ids_right_re_nxt = 'b0;//'b1;
            ids_right_addr_nxt = ids_right_if.addr;
            ids_right_we_nxt = 'b1;

            //state_finish_cleanup();

             `ifdef DEBUG
             $display("Start");
            // display_debug_ids(iteration_ctr);
             `endif
          end

          `ifdef DEBUG
          if(ids_right_if.addr >= 0 && ids_right_if.addr < SYMBOL_QTY)
            debug[ids_right_if.addr] <= ids_right_if.in;
          `endif

        end

        PRE_IDS_L2R: begin
          delay_state_nxt = delay_state +1;
          if(delay_state == 3) begin
            ids_source_nxt = IDS_L2R;
            ids_left_re_nxt = 'b1;
            ids_right_re_nxt = 'b0;
            ids_left_addr_nxt = 'b0;
            ids_left_we_nxt = 'b0;
            state_finish_cleanup();
          //  display_debug_ids(iteration_ctr);
          end else begin
            ids_source_nxt = PRE_IDS_L2R;
            ids_left_re_nxt = 'b0;
            ids_right_re_nxt = 'b0;
            ids_left_addr_nxt = ids_left_if.addr;
            ids_left_we_nxt = 'b1;
          end
        end

        IDS_L2R: begin
          ids_source_nxt = IDS_L2R;
          ids_seq_read_ready = ids_left_if.ready;

          save_to_fifo(0);
          fifo2dna_mem();
          dna_mem2fifo_dna();
          mem_write_when_ready(dna_read_fifo_valid,
                               ids_right_if.ready && dna_fifo_write_started && !dna_read_fifo_re,
                               dna_read_fifo_dout[IDS_ADDR_WIDTH+1:IDS_ADDR_WIDTH],
                               dna_fifo_re_nxt);

          if(ids_right_if.we && (!ids_right_if.ready)) begin
            ids_right_we_nxt = 'b1;
          end else begin
            ids_right_we_nxt = dna_fifo_valid_n1;
          end

          next_id = pos_ctr[char_prev]-1;
          ids_right_addr_nxt = next_id;
          ids_right_in_nxt = dna_prev_addr;
          written_symbols_ctr_nxt = dna_fifo_valid_n1 ? written_symbols_ctr +1 : written_symbols_ctr;

           // if(ids_right_we_nxt)
           //   $display("L2R addr %0d   id %0d",ids_right_addr_nxt, ids_right_in_nxt);

          if(SYMBOL_QTY == written_symbols_ctr) begin
            ids_source_nxt = PRE_IDS_R2L;
            ids_left_re_nxt = 'b0;
            ids_right_re_nxt ='b0;//'b1;
            ids_right_addr_nxt = ids_right_if.addr;
            ids_right_we_nxt = 'b1;
            delay_state_nxt = 'b0;

           // state_finish_cleanup();

             `ifdef DEBUG
           //  $display("L2R");
            // display_debug_ids(iteration_ctr);
             `endif
          end

          `ifdef DEBUG
           // if(ids_right_if.addr >= 0 && ids_right_if.addr < SYMBOL_QTY)
           //   debug[ids_right_if.addr] <= ids_right_if.in;

          if(ids_right_addr_nxt >= 0 && ids_right_addr_nxt < SYMBOL_QTY && ids_right_we_nxt)
            debug[ids_right_addr_nxt] <= ids_right_in_nxt;
          `endif
        end

        PRE_IDS_R2L: begin
            delay_state_nxt = delay_state +1;
            if(delay_state == 3) begin
              delay_state_nxt = 'b0;
              ids_source_nxt = IDS_R2L;
              ids_left_re_nxt = 'b0;
              ids_right_re_nxt = 'b1;
              ids_right_addr_nxt = 'b0;
              ids_right_we_nxt = 'b0;
              state_finish_cleanup();
          //    display_debug_ids(iteration_ctr);
            end
            else begin
              ids_source_nxt = PRE_IDS_R2L;
              ids_left_re_nxt = 'b0;
              ids_right_re_nxt = 'b0;
              ids_right_addr_nxt = ids_right_if.addr;
              ids_right_we_nxt = 'b1;
            end


        end

        IDS_R2L: begin
          ids_source_nxt = IDS_R2L;
          ids_seq_read_ready = ids_right_if.ready;

          save_to_fifo(1);
          fifo2dna_mem();
          dna_mem2fifo_dna();
          mem_write_when_ready(dna_read_fifo_valid,
                               ids_left_if.ready && dna_fifo_write_started && !dna_read_fifo_re,
                               dna_read_fifo_dout[IDS_ADDR_WIDTH+1:IDS_ADDR_WIDTH],
                               dna_fifo_re_nxt);

          if(ids_left_if.we && (!ids_left_if.ready)) begin
            ids_left_we_nxt = 'b1;
          end else begin
            ids_left_we_nxt = dna_fifo_valid_n1;
          end

          next_id = pos_ctr[char_prev]-1;
          ids_left_addr_nxt = next_id;
          ids_left_in_nxt = dna_prev_addr;
          written_symbols_ctr_nxt = dna_fifo_valid_n1 ? written_symbols_ctr +1 : written_symbols_ctr;

           // if(ids_left_we_nxt)
           //   $display("R2L addr %0d   id %0d",ids_left_addr_nxt, ids_left_in_nxt);

          if(SYMBOL_QTY == written_symbols_ctr) begin
            ids_source_nxt = PRE_IDS_L2R;
            ids_left_re_nxt = 'b1;
            ids_right_re_nxt = 'b0;
            ids_left_addr_nxt = ids_left_if.addr;
            ids_left_we_nxt = 'b1;
            delay_state_nxt = 'b0;
            //state_finish_cleanup();

             `ifdef DEBUG
            // $display("R2L");
          //   display_debug_ids(iteration_ctr);
             `endif
          end

          `ifdef DEBUG
          // if(ids_left_if.addr >= 0 && ids_left_if.addr < SYMBOL_QTY)
          //   debug[ids_left_if.addr] <= ids_left_if.in;
          if(ids_left_addr_nxt >= 0 && ids_left_addr_nxt < SYMBOL_QTY && ids_left_we_nxt)
            debug[ids_left_addr_nxt] <= ids_left_in_nxt;
          `endif
        end

        IDS_FINISHED: begin
          /* stop signaling BWT valid and prepare for next data upcoming when DNA_memory is being written */
          ids_source_nxt = DNA_if.we ? IDS_IDLE : IDS_FINISHED;
          idle();
          for (int ik = 0; ik < SYMBOL_QTY; ik++) begin
            debug_fb[ik] <= debug[ik];
          end
          debug_fb[SYMBOL_QTY] <= iteration_ctr;
        end

      endcase
      
      crc_update();
      finish_handle();

    end

  reg ids_mem_write_started = 0;
  reg ids_fifo_write_started = 0;
  reg ids_fifi2dna_mem_re = 0;


  function void gen2fifo_dna();
  begin
    if(DNA_if.out_addr == SYMBOL_QTY-1) begin
      dna_fifo_write_started = 'b1;
    end

    if((dna_if_out_addr_prev != DNA_if.out_addr) ||
       (!dna_valid_prev && DNA_if.valid)) begin
      if(dna_fifo_write_started) writing_dna_fifo = 'b1;
    end

    if((SYMBOL_QTY-1 == DNA_if.out_addr) && (written_symbols_ctr > SYMBOL_QTY/2)) begin
      writing_dna_fifo = 'b0;
      dna_fifo_write_started = 'b0;
    end
  end
  endfunction

  function void save_to_fifo(logic dir);
  begin
    ids_read_fifo_we = 'b0;
    if(dir) begin
      if(ids_right_if.valid) begin
        if((ids_right_out_addr_prev != ids_right_if.out_addr) || !ids_right_valid_prev) begin
          ids_read_fifo_we = 'b1;
          ids_fifo_write_started = 'b1;
          //ids_read_fifo_din = {ids_right_if.out,ids_right_if.out_addr};
          ids_read_fifo_din = ids_right_if.out;
        end
      end
    end else begin
      if(ids_left_if.valid) begin
        if((ids_left_out_addr_prev != ids_left_if.out_addr) || !ids_left_valid_prev) begin
          ids_read_fifo_we = 'b1;
          ids_fifo_write_started = 'b1;
          //ids_read_fifo_din = {ids_left_if.out,ids_left_if.out_addr};
          ids_read_fifo_din = ids_left_if.out;
        end
      end
    end

  end
  endfunction

  function void fifo2dna_mem();
  begin
      if(DNA_if.ready && ids_fifo_write_started) begin
        ids_read_fifo_re_nxt = 'b1;
      end else begin
        ids_read_fifo_re_nxt = 'b0;
      end

      if(ids_read_fifo_valid) begin
        ids_fifi2dna_mem_re = 'b1;
        //ids_right_out_buf = ids_read_fifo_dout[(2*IDS_ADDR_WIDTH)-1:IDS_ADDR_WIDTH];
        //ids_left_out_buf = ids_read_fifo_dout[(2*IDS_ADDR_WIDTH)-1:IDS_ADDR_WIDTH];
        ids_right_out_buf = ids_read_fifo_dout[IDS_ADDR_WIDTH-1:0];
        ids_left_out_buf = ids_read_fifo_dout[IDS_ADDR_WIDTH-1:0];
      end
  end
  endfunction

  function void dna_mem2fifo_dna();
  begin
      if(ids_fifi2dna_mem_re) begin
        if(dna_if_out_addr_prev != DNA_if.out_addr) begin
          writing_dna_fifo = 'b1;
          dna_fifo_write_started = 'b1;
        end else if (!dna_valid_prev && DNA_if.valid) begin
          writing_dna_fifo = 'b1;
          dna_fifo_write_started = 'b1;
        end

      end 
  end
  endfunction


  function void mem_write_init(input logic start);
  begin
    if(start) begin
      ids_mem_write_started = 'b1;
      pos_ctr_nxt[A] = 'b0;
      pos_ctr_nxt[C] = C_pos;
      pos_ctr_nxt[G] = G_pos;
      pos_ctr_nxt[T] = T_pos;
      all_pos_ctr_nxt = 'b0;
    end
  end
  endfunction

  function void state_finish_cleanup();
  begin
      ids_fifi2dna_mem_re = 'b0;
      written_symbols_ctr_nxt = 'b0;
      state_change_nxt = 'b1;
      ids_seq_read_rst = 'b1;
      dna_read_fifo_rst_nxt = 'b1;
      ids_right_out_buf = 'b0;
      ids_left_out_buf = 'b0;
      dna_fifo_re_nxt = 'b0;
      writing_dna_fifo = 'b0;
      dna_fifo_write_started = 'b0;
      mem_write_init(1);
  end
  endfunction

  function void mem_write_when_ready(input logic fifo_valid,
                                     input logic mem_ready,
                                     input logic [1:0] dna_char,
                                     output logic fifo_re
                                     );
  begin

    if(ids_mem_write_started) begin

      if(fifo_valid) begin
        pos_ctr_nxt[A] = pos_ctr[A];
        pos_ctr_nxt[C] = pos_ctr[C];
        pos_ctr_nxt[G] = pos_ctr[G];
        pos_ctr_nxt[T] = pos_ctr[T];
        pos_ctr_nxt[dna_char] = pos_ctr[dna_char]+1;
        all_pos_ctr_nxt = all_pos_ctr+1;
      end else begin
        pos_ctr_nxt[A] = pos_ctr[A];
        pos_ctr_nxt[C] = pos_ctr[C];
        pos_ctr_nxt[G] = pos_ctr[G];
        pos_ctr_nxt[T] = pos_ctr[T];
        all_pos_ctr_nxt = all_pos_ctr;
      end

      if(mem_ready) begin
        fifo_re = 'b1;
      end else begin
        fifo_re = 'b0;
      end

    end else begin
      fifo_re = 'b0;
    end
  end
  endfunction

  function void finish_handle();
  begin 
    if (ids_source != IDS_FINISHED) begin
      ids_right_valid_nxt = 'b0;
      ids_left_valid_nxt = 'b0;
    end

    if(state_change && ids_left_crc32 == ids_right_crc32 && ids_left_crc32 != 32'hFFFF_FFFF) 
    begin
      ids_right_valid_nxt = 'b1;
      ids_left_valid_nxt = 'b1;
      ids_source_nxt = IDS_FINISHED;
     // display_debug_ids();
    end 
    else if((SYMBOL_QTY <= iteration_ctr) && state_change_nxt)
    begin
      ids_source_nxt = IDS_FINISHED;
      //display_debug_ids();
      if(ids_source == IDS_L2R) begin
        ids_right_valid_nxt = 'b1;
        ids_left_valid_nxt = 'b0;
      end else if (ids_source == IDS_R2L) begin
        ids_right_valid_nxt = 'b0;
        ids_left_valid_nxt = 'b1;
      end
    end
  end
  endfunction

  function  void pre_calc_cleanup();
  begin
      delay_state_nxt = delay_state;
      state_change_nxt = 'b0;
      dna_fifo_re_nxt = 'b0;
      written_symbols_ctr_nxt = 'b0;
      ids_right_we_nxt = 'b0;
      ids_left_we_nxt = 'b0;
      ids_read_fifo_re_nxt = 'b0;
      dna_read_fifo_rst_nxt = 'b0;
      writing_dna_fifo = 'b0;
      ids_seq_read_rst = 'b0;
  end
  endfunction

  function void crc_update();
  begin
    /* calculates crc basing on data saved to ids_memory.
       init to 0xFFF... when starting calculating new polynomial */ 
    ids_left_crc32_nxt = ids_left_crc32;
    ids_right_crc32_nxt = ids_right_crc32;

    if(!ids_right_if.re && !all_pos_ctr) begin
      ids_right_crc32_nxt = 32'hFFFF_FFFF;
    end else if(ids_right_if.we) begin
      ids_right_crc32_nxt = nextCRC32_D2(ids_right_if.in,ids_right_crc32);
    end
         
    if(!ids_left_if.re && !all_pos_ctr) begin
       ids_left_crc32_nxt = 32'hFFFF_FFFF;
    end else if (ids_left_if.we) begin
      ids_left_crc32_nxt  = nextCRC32_D2(ids_left_if.in,ids_left_crc32);
    end

  end
  endfunction

  function void idle();
  begin
    ids_source_nxt = IDS_IDLE;
    ids_left_we_nxt = 0;
    ids_right_we_nxt = 0;
    pos_ctr_nxt[A] = 'b0;
    pos_ctr_nxt[C] = 'b0;
    pos_ctr_nxt[G] = 'b0;
    pos_ctr_nxt[T] = 'b0;
    all_pos_ctr_nxt = 'b1;
    if(last_char) begin
      ids_source_nxt = IDS_GENERATE;
    end
  end
  endfunction

  function logic [IDS_ADDR_WIDTH-1:0] get_prev_symbol(input reg[IDS_ADDR_WIDTH-1:0] symbol, int n_cycles);
  begin
    if(symbol < n_cycles) begin
      get_prev_symbol = SYMBOL_QTY - n_cycles + symbol;
    end else begin
      get_prev_symbol = symbol - n_cycles;
    end
  end
  endfunction : get_prev_symbol

  function logic [IDS_ADDR_WIDTH-1:0] get_next_symbol(input reg[IDS_ADDR_WIDTH-1:0] symbol, int n_cycles);
  begin
      get_next_symbol = (symbol + n_cycles)%SYMBOL_QTY;
  end
  endfunction : get_next_symbol

  function void display_debug_ids(int iter);
  begin
    $display("Iter %d",iter); //newline
    for(int i = 0; i < SYMBOL_QTY; i++) $write("%0d ",debug[i]);
    $display(""); //newline
  end
  endfunction

endmodule
