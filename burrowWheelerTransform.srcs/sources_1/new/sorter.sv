`timescale 1ns / 1ps
`include "dna_representation.vh"

module sorter
#( parameter SYMBOL_QTY = 10,
   parameter SYMBOL_IDX_WIDTH = 4)
(
   input wire [1:0] DNA_in[SYMBOL_QTY],
   output reg [1:0] DNA_out[SYMBOL_QTY],
   
   output reg [SYMBOL_IDX_WIDTH-1:0] ids_out[SYMBOL_QTY] ,

   input wire [SYMBOL_IDX_WIDTH-1:0] C_pos,
   input wire [SYMBOL_IDX_WIDTH-1:0] G_pos,
   input wire [SYMBOL_IDX_WIDTH-1:0] T_pos,

   input wire valid_in,
   output reg valid_out,

   input wire clock,
   input wire reset
);

    reg [SYMBOL_IDX_WIDTH-1:0] symbol_positions[SYMBOL_QTY][3]; // 4 symbols x (pipeline_stages=symbol_qty)
    reg [SYMBOL_IDX_WIDTH-1:0] ids    [SYMBOL_QTY][SYMBOL_QTY]; 
    reg [SYMBOL_IDX_WIDTH-1:0] ids_nxt[SYMBOL_QTY][SYMBOL_QTY]; 
    reg [1:0] DNAs[SYMBOL_QTY][SYMBOL_QTY];
    reg valids [SYMBOL_QTY];


    assign symbol_positions[0][0] = C_pos;  
    assign symbol_positions[0][1] = G_pos; 
    assign symbol_positions[0][2] = T_pos; 

    assign DNAs[0] = DNA_in;
    assign valids[0] = valid_in;

    always @(posedge clock)
    begin
    	if(reset) begin
    		valid_out <= 'b0;
    		DNA_out <= '{default:0};
    		ids_out <= '{default:0};
    	end else begin
    		ids_out <= ids[SYMBOL_QTY-1];
    		valid_out <= valids[SYMBOL_QTY-1];
    		DNA_out <= DNAs[SYMBOL_QTY-1];
        	symbol_qty_pipe();
        	fill_first_ids(ids[0]);
        end
    end

    always @*
    begin
    	for(int ids_stage=0; ids_stage < SYMBOL_QTY; ids_stage+=1)
    	begin
    		//ids_nxt[ids_stage] = ids[ids_stage];
    		calc_new_ids(ids_nxt[ids_stage],
    			             ids[ids_stage],
    			symbol_positions[ids_stage],
    			                 ids_stage);
    	end
   end

    function automatic void calc_new_ids(
    	output reg [SYMBOL_IDX_WIDTH-1:0] ids_nxt[SYMBOL_QTY],
    	input reg  [SYMBOL_IDX_WIDTH-1:0] ids[SYMBOL_QTY],
    	input reg [SYMBOL_IDX_WIDTH-1:0] symbol_positions[3],
    	input reg [SYMBOL_IDX_WIDTH-1:0] stage
    	);
    begin
    	automatic reg [SYMBOL_IDX_WIDTH-1:0] pos_ctr[4];
		automatic reg [SYMBOL_IDX_WIDTH-1:0] current_id;
		automatic reg [1:0] ch;

	    pos_ctr[A] = 'b0;
	    pos_ctr[C] = symbol_positions[0];
	    pos_ctr[G] = symbol_positions[1];
	    pos_ctr[T] = symbol_positions[2];
	    for(int symbol=0; symbol < SYMBOL_QTY; symbol+=1)
        begin
        	current_id = (ids[symbol] == 0) ? (SYMBOL_QTY-1) : (ids[symbol]-1);
    		ch = get_char(current_id,stage);
    		// $write("id %0d, ch %0d", current_id, ch);
    		// print_char(ch);
    		// $display(""); // newline
    		case(ch)
    		A: begin
               ids_nxt[pos_ctr[A]] = current_id;
               pos_ctr[A]+=1;
               end
            C: begin 
               ids_nxt[pos_ctr[C]] = current_id;
               pos_ctr[C]+=1;
               end
            G: begin 
               ids_nxt[pos_ctr[G]] = current_id;     
               pos_ctr[G]+=1;
               end
            T: begin 
               ids_nxt[pos_ctr[T]] = current_id;         
               pos_ctr[T]+=1;
               end                             
            endcase         
    	end 
    end
    endfunction

    task symbol_qty_pipe();
    fork
    	for (int i = 0; i < SYMBOL_QTY-1; i++) fork
    		symbol_positions[i+1] <=symbol_positions[i];
    		ids[i+1] <=ids_nxt[i];
    		DNAs[i+1] <=DNAs[i];
    		valids[i+1] <= valids[i];
    	join
    join
    endtask : symbol_qty_pipe

    task fill_first_ids(output reg [SYMBOL_IDX_WIDTH-1:0] ids_fist[SYMBOL_QTY]);
    fork
        for(int symbol=SYMBOL_QTY-1; symbol>=0; symbol-=1)
        fork
            ids_fist[symbol] = symbol;
        join
    join
    endtask

    function automatic [1:0] get_char(reg [SYMBOL_IDX_WIDTH-1:0] id, 
    	                              reg [SYMBOL_IDX_WIDTH-1:0] pipe_stage);
    begin
        return(DNAs[pipe_stage][id]);
    end
    endfunction

    function display_ids(reg [SYMBOL_IDX_WIDTH-1:0] ids [SYMBOL_QTY]);
    begin
       $write("ids = ");
       for(int symbol=SYMBOL_QTY-1; symbol>=0; symbol-=1)
       begin
          $write("%0d, ",ids[symbol]);
       end
       $display(""); // newline
    end
    endfunction


endmodule
