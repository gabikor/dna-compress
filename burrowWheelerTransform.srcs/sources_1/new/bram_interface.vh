`ifndef BRAM_INTERFACE_VH
`define BRAM_INTERFACE_VH

interface bram_ids_if;
    logic [11:0] in;
    logic [11:0] out;
    logic [11:0] addr;
    logic [11:0] out_addr;
    logic        we;
    logic        re; /* read enable */
    logic        valid;
    logic        ready;
endinterface : bram_ids_if

interface bram_DNA_if;
    logic [1:0] in;
    wire  [1:0] out;
    logic [11:0] addr;
    logic [11:0] out_addr;
    logic        we;
    logic        re; /* read enable */
    logic        valid;
    logic        ready;
endinterface : bram_DNA_if

`endif