`timescale 1ns / 1ps

module mem_sequential_read
#(parameter WIDTH,
  parameter SYMBOL_QTY)
(
	input wire clk,
	input wire rst,
	input wire ready,
	input reg [WIDTH-1:0] start_val,
	output reg [WIDTH-1:0] addr
);

reg [WIDTH-1:0] addr_nxt;

always @(posedge clk)
begin
	if(rst)
	   addr <= start_val;
	else
       addr <= addr_nxt;
end

always_comb begin
	if(ready && (addr < SYMBOL_QTY-1)) begin
		addr_nxt = addr+1;
	end else begin
        addr_nxt = addr;
	end
end


endmodule
