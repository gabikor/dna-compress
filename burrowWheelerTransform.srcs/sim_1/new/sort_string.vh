`ifndef SORT_STRING_VH
`define SORT_STRING_VH
    

function string to_str(reg [1:0] chars [SYMBOL_QTY]);
begin
    automatic string ret = "";
    for(int i = 0; i < SYMBOL_QTY; i+=1)
    begin
    case(chars[i])
        A:ret = {ret,"A"};
        C:ret = {ret,"C"};
        G:ret = {ret,"G"};
        T:ret = {ret,"T"};
    endcase
    end
    return(ret);
end
endfunction

// A utility function to swap two elements 
function automatic void swap(ref string a,ref string b);
begin
    string t = a; 
    a = b; 
    b = t; 
end
endfunction 
  
/* This function takes last element as pivot, places 
   the pivot element at its correct position in sorted 
    array, and places all smaller (smaller than pivot) 
   to left of pivot and all greater elements to right 
   of pivot */
function automatic int partition (/*ref string arr[], */int low, int high);
begin
    string pivot = bwt_shifts[high];    // pivot 
    int i = (low - 1);  // Index of smaller element 
  
    for (int j = low; j <= high- 1; j+=1) 
    begin
        // If current element is smaller than the pivot 
        if (bwt_shifts[j] < pivot) 
        begin 
            i+=1;    // increment index of smaller element 
            swap(bwt_shifts[i], bwt_shifts[j]); 
        end 
    end 
    swap(bwt_shifts[i + 1], bwt_shifts[high]); 
    return (i + 1); 
end
endfunction
  
/* The main function that implements QuickSort 
 arr[] --> Array to be sorted, 
  low  --> Starting index, 
  high  --> Ending index */
function automatic  void quickSort(/*ref string arr[SYMBOL_QTY],*/ int low, int high) ;
begin 
    if (low < high) 
    begin
        /* pi is partitioning index, arr[p] is now 
           at right place */
        int pi = partition(/*arr,*/ low, high); 
  
        // Separately sort elements before 
        // partition and after partition 
        quickSort(/*bwt_shifts,*/ low, pi - 1); 
        quickSort(/*bwt_shifts,*/ pi + 1, high); 
    end 
end
endfunction 

`endif