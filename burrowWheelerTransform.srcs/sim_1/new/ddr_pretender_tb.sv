`timescale 1ns / 1ps
`include "bram_interface.vh"

module ext_mem_pretender_tb();

localparam ART_DELAY = 3; /* artificail delay */

bram_DNA_if mem_if();
reg clk, reset, read_en;

initial begin
    clk = 'b0;
    reset = 'b1;
    #5
    clk = #5 ~clk;
    clk = #5 ~clk;
    reset = 'b0;
    repeat(100) clk = #5 ~clk;
end

initial
begin
	repeat(4) @(posedge clk);
	for (int i = 0; i < 5; i++) begin
		preload_DNA(i,i);
	end
	for (int i = 0; i < 5; i++) begin
		read_DNA(i);
	end
end

always @(posedge clk) 
begin
	if(mem_if.out_valid)
		$display("mem read %0d",mem_if.out);
end

ext_mem_pretender
#(.ART_DELAY(ART_DELAY),
  .MEMORY_WIDTH(17),
  .DATA_WIDTH(2))
is_it_ddr (
	.clock(clk),
	.reset(reset),
	.mem_if(mem_if)
);


task preload_DNA(logic [16:0] addr, logic [1:0] data);
begin
	mem_if.addr = addr;
	mem_if.in = data;
	mem_if.we = 'b1;
	repeat(ART_DELAY) @(posedge clk);
	mem_if.we = 'b0;
end
endtask

task read_DNA(logic [16:0] addr);
begin
	mem_if.re = 'b1;
	mem_if.addr = addr;
	@(posedge clk);
	mem_if.re = 'b0;
	repeat(ART_DELAY) @(posedge clk);
end
endtask

endmodule
