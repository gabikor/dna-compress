`timescale 1ns / 1ps
`include "bram_interface.vh"


module ext_mem_ids_tb();

localparam ART_DELAY = 3; /* artificail delay */

bram_ids_if mem_if();
reg clk, reset;
reg [2:0] test_step;
reg mem_if_re_nxt;
reg [31:0] mem_if_addr_nxt;

reg [31:0] rand_idx=0, rand_idx_nxt = 0;

const int random_address[20] = {18, 12, 10, 8, 9, 3, 15, 2, 14, 19, 17, 16, 4, 0, 13, 6, 7, 1, 11, 5};

initial begin
    clk = 'b0;
    reset = 'b1;
    #5
    clk = #5 ~clk;
    clk = #5 ~clk;
    reset = 'b0;
    forever clk = #5 ~clk;
end

initial
begin
	repeat(4) @(posedge clk);
	test_step = 0;
	for (int i = 0; i < 5; i++) begin
		preload_DNA(i,i);
		preload_DNA(i+5,i+5);
		preload_DNA(i+10,i+10);
		preload_DNA(i+15,i+15);
	end
	/* sequence */
	test_step = 1;
	for (int i = 0; i < 5; i++) begin
		read_DNA(i);
		read_DNA(i+5);
		read_DNA(i+10);
		read_DNA(i+15);
	end
	test_step = 0;
	/* same bank */
	repeat(6) @(posedge clk);
	test_step = 2;
	for (int i = 0; i < 20; i++) begin
		read_DNA(i);
	end
	test_step = 0;

	/* same bank with 'ready' checking */
	repeat(6) @(posedge clk);
	mem_if.addr = 0;
	test_step = 3;
	/* reading in always block */


	/* reading in always block */
	repeat(60) @(posedge clk);
	test_step = 0;
	repeat(6) @(posedge clk);
	mem_if.addr = 0;
	test_step = 4;

	/* reading in always block */
	repeat(60) @(posedge clk);
	test_step = 0;

end


always @(posedge clk) 
begin
	if(mem_if.valid)
		$display("mem read %0d",mem_if.out);
end

always @(posedge clk)
begin
	if(3 == test_step) begin
       mem_if.re <= mem_if_re_nxt;
       mem_if.addr <= mem_if_addr_nxt;
	end
	if(4 == test_step) begin
       mem_if.re <= mem_if_re_nxt;
       mem_if.addr <= mem_if_addr_nxt;
       rand_idx <= rand_idx_nxt;
	end
end

reg [31:0] temp;

always_comb begin
	mem_if_addr_nxt = 0;
	mem_if_re_nxt = 1;
	if(3 == test_step) begin
		if(mem_if.ready) begin
			mem_if.we = 'b0;
			mem_if_addr_nxt = mem_if.addr+1;
			mem_if_re_nxt = 'b1;
		end else begin
            mem_if_addr_nxt = mem_if.addr;
		end
	end

	if(4 == test_step) begin
		if(mem_if.ready) begin
			mem_if.we = 'b0;
			rand_idx_nxt = rand_idx +1;
			temp = random_address[rand_idx];
			mem_if_addr_nxt  = temp;
			mem_if_re_nxt = 'b1;
		end else begin
            mem_if_addr_nxt = mem_if.addr;
            rand_idx_nxt = rand_idx;
		end
	end


end

ext_mem_arbiter_ids
#(.ART_DELAY(ART_DELAY),
  .MEMORY_WIDTH(16),
  .MAX_SYMBOL_NO(20),
  .BANK_NO(4))
arbiter (
	.clock(clk),
	.reset(reset),
	.main_mem_if(mem_if)
);


task preload_DNA(logic [31:0] addr, logic [31:0] data);
begin
	mem_if.addr = addr;
	mem_if.in = data;
	mem_if.we = 'b1;
	mem_if.re = 'b0;
	/*repeat(ART_DELAY)*/ @(posedge clk);
	mem_if.we = 'b0;
end
endtask

task read_DNA(logic [31:0] addr);
begin
	mem_if.addr = addr;
	mem_if.re = 'b1;
	mem_if.we = 'b0;
	/*repeat(ART_DELAY)*/ @(posedge clk);
	mem_if.re = 'b0;
end
endtask

task read_DNA_when_ready(logic [31:0] addr);
begin
	mem_if.addr = addr;
	mem_if.we = 'b0;
	while(!mem_if.ready) begin
		@(posedge clk);
	end
	mem_if.re = 'b1;
	@(posedge clk);
	mem_if.re = 'b0;

end
endtask

endmodule
