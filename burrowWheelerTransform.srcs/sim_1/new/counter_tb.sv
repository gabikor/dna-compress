`timescale 1ns / 1ps

module counter_tb();

	reg clk, reset;
	reg [31:0] ctr;

	initial begin
	    clk = 'b0;
	    reset = 'b1;
	    #5
	    clk = #5 ~clk;
	    clk = #5 ~clk;
	    reset = 'b0;
	    forever clk = #5 ~clk;
	end


    counter_by_bank
    #(.MAX_COUNT(20),
      .SYM_PER_BANK(5),
      .WIDTH(32),
      .BANK_NO(4))
    bank_aware_ctr (
      .count(ctr),
      .reset(reset),
      .clock(clk)
    );


endmodule : counter_tb