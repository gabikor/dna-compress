`timescale 1ns / 1ps
`include "dna_representation.vh"
`include "bram_interface.vh"

module top_tb(

    );


reg clk, reset, start;
reg [2:0] btn;
wire [3:0] led;

initial begin
    clk = 'b0;
    reset = 'b0;
    start = 'b0;
    #5
    clk = #5 ~clk;
    clk = #5 ~clk;
    reset = 'b0;
    /*forever*/ repeat(500) clk = #5 ~clk;
    reset = 'b1;
    start = 'b1;
    repeat(5500) clk = #5 ~clk;
    reset = 'b0;
    start = 'b0;
    forever clk = #5 ~clk;
end

dna_compress_top top_mod
(
	.btn(btn),
	.clock(clk),
	.reset(reset),
	.start(start),
	.led(led)
);

endmodule
