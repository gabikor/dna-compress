/******************************************************************************
*
* Copyright (C) 2009 - 2014 Xilinx, Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining a copy
* of this software and associated documentation files (the "Software"), to deal
* in the Software without restriction, including without limitation the rights
* to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
* copies of the Software, and to permit persons to whom the Software is
* furnished to do so, subject to the following conditions:
*
* The above copyright notice and this permission notice shall be included in
* all copies or substantial portions of the Software.
*
* Use of the Software is limited solely to applications:
* (a) running on a Xilinx device, or
* (b) that interact with a Xilinx device through a bus or interconnect.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
* XILINX  BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
* WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF
* OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
* SOFTWARE.
*
* Except as contained in this notice, the name of the Xilinx shall not be used
* in advertising or otherwise to promote the sale, use or other dealings in
* this Software without prior written authorization from Xilinx.
*
******************************************************************************/

/*
 * helloworld.c: simple test application
 *
 * This application configures UART 16550 to baud rate 9600.
 * PS7 UART (Zynq) is not initialized by this application, since
 * bootrom/bsp configures it to baud rate 115200
 *
 * ------------------------------------------------
 * | UART TYPE   BAUD RATE                        |
 * ------------------------------------------------
 *   uartns550   9600
 *   uartlite    Configurable only in HW design
 *   ps7_uart    115200 (configured by bootrom/bsp)
 */

#include <stdio.h>
#include "xparameters.h"
#include "platform.h"
#include "xil_printf.h"
#include "sleep.h"
#include "xil_io.h"
#include "xuartps_hw.h"

u32 id = 0;
u32 reset = 0;
u32 id_prev = 0;
uint8_t burst = 1;
u32 read_40[40] = {0};
u32 *read_40_ptr = 0;
u32 last_read = 0;

int main()
{
    init_platform();
    xil_printf("\r\n BWT debug app \n\r");

    Xil_Out32((u32)(XPAR_AXI_GPIO_0_BASEADDR + 0x4),(u32)0xFFFFFFFF);
    id = Xil_In32(XPAR_AXI_GPIO_0_BASEADDR);
    xil_printf("id %x\n\r",id>>1);
    read_40_ptr = read_40;

    while(1) {
    	id = Xil_In32(XPAR_AXI_GPIO_0_BASEADDR);

//    	if(XUartPs_IsReceiveData(STDOUT_BASEADDRESS)) {
//    		burst = !burst;
//    		XUartPs_RecvByte(STDOUT_BASEADDRESS);
//    	}
//    	if(!burst) {
//        	xil_printf("ID = %d, bwt_ready = %d\n\r",id>>1,id&0x1);
//    		sleep(1);
//    	} else
    	if(id_prev != id){
    		reset = id&(1<<31);

    		if(reset) {
    			*read_40_ptr = (id>>1)&0x3FFFFFFF;
    			//xil_printf("id %x\n\r",*read_40_ptr);
    			if(read_40_ptr < &read_40[39]) read_40_ptr++;
    			last_read++;
    		}else {
    			read_40_ptr = read_40;
    			for(u32 i = 0; i< last_read; i++){
    				xil_printf("id %x\n\r",read_40[i]);
    			}
    			last_read = 0;
    		}

    	}
    	id_prev = id;
    }

    cleanup_platform();
    return 0;
}
