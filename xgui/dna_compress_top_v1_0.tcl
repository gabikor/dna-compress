# Definitional proc to organize widgets for parameters.
proc init_gui { IPINST } {
  ipgui::add_param $IPINST -name "Component_Name"
  #Adding Page
  ipgui::add_page $IPINST -name "Page 0"

  ipgui::add_param $IPINST -name "SYMBOL_QTY"
  ipgui::add_param $IPINST -name "IDS_ADDR_WIDTH"

}

proc update_PARAM_VALUE.IDS_ADDR_WIDTH { PARAM_VALUE.IDS_ADDR_WIDTH } {
	# Procedure called to update IDS_ADDR_WIDTH when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.IDS_ADDR_WIDTH { PARAM_VALUE.IDS_ADDR_WIDTH } {
	# Procedure called to validate IDS_ADDR_WIDTH
	return true
}

proc update_PARAM_VALUE.SYMBOL_QTY { PARAM_VALUE.SYMBOL_QTY } {
	# Procedure called to update SYMBOL_QTY when any of the dependent parameters in the arguments change
}

proc validate_PARAM_VALUE.SYMBOL_QTY { PARAM_VALUE.SYMBOL_QTY } {
	# Procedure called to validate SYMBOL_QTY
	return true
}


