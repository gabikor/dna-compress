onbreak {quit -force}
onerror {quit -force}

asim -t 1ps +access +r +m+BRAM_DNA -L xil_defaultlib -L xpm -L xilinx_vip -L unisims_ver -L unimacro_ver -L secureip -O5 xil_defaultlib.BRAM_DNA xil_defaultlib.glbl

do {wave.do}

view wave
view structure

do {BRAM_DNA.udo}

run -all

endsim

quit -force
