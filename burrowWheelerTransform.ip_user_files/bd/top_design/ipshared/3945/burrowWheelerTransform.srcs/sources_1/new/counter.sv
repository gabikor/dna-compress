`timescale 1ns / 1ps

module counter
#( parameter MAX_COUNT,
   parameter WIDTH,
   parameter START_VAL)
(
    output reg [WIDTH-1:0] count,
    output reg change,
    input en,
    input reset,
    input clock
);

   reg [WIDTH-1:0] ctr_nxt;
   reg change_nxt;

    always @(posedge clock)
        if (reset) begin
            count <= START_VAL;
            change <= 'b1;
        end else begin
            count <= ctr_nxt;
            change <= change_nxt;
        end

    always @* 
    begin
      if(en) begin
        change_nxt = 'b1;
        ctr_nxt = (MAX_COUNT-1 == count) ? 'b0 : count + 1'b1;
      end else begin
        change_nxt = 'b0;
        ctr_nxt = count;
      end
    end

endmodule
