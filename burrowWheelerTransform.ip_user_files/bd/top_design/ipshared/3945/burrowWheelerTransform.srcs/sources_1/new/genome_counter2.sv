`timescale 1ns / 1ps
`include "dna_representation.vh"

module genome_counter2
#(parameter SYMBOL_QTY = 10,
  parameter IDS_ADDR_WIDTH = 12)
(
    input wire [1:0] DNA_in,
    output reg [(IDS_ADDR_WIDTH-1):0] C_pos,
    output reg [(IDS_ADDR_WIDTH-1):0] G_pos,
    output reg [(IDS_ADDR_WIDTH-1):0] T_pos,
    output reg [(IDS_ADDR_WIDTH-1):0] X_pos,
    output reg [IDS_ADDR_WIDTH-1:0] all_genes_qty,
    /* no need to count T's */
    output reg valid_out,
    input wire first_char,
    input wire clock,
    input wire reset
);
 
    reg [IDS_ADDR_WIDTH-1:0] A_qty, C_qty, G_qty, X_qty;
    reg [IDS_ADDR_WIDTH-1:0] all_genes_qty_nxt;
    reg valid_enable;

	assign C_pos = valid_out ? A_qty : C_pos;
	assign G_pos = valid_out ? C_qty + A_qty : G_pos;
	assign T_pos = valid_out ? G_qty + C_qty + A_qty : T_pos;  
    assign X_pos = valid_out ? X_qty : X_pos;
        
    always @(posedge clock)
    begin
        if(reset) begin
            A_qty <= 'b0;
            C_qty <= 'b0;
            G_qty <= 'b0;
            X_qty <= 'b0;
            all_genes_qty <= 'b0;
            //valid_enable <= 'b0;
        end else if(first_char) begin
            A_qty <= (A == DNA_in);
            C_qty <= (C == DNA_in);
            G_qty <= (G == DNA_in);
            X_qty <= ((A != DNA_in) || (C != DNA_in) || (G != DNA_in) || (T != DNA_in));
            all_genes_qty <= 'b1;
            // valid_enable <= 'b1;
        end else begin
            A_qty <= A_qty + (A == DNA_in);
            C_qty <= C_qty + (C == DNA_in);
            G_qty <= G_qty + (G == DNA_in);
            X_qty <= X_qty + ((A != DNA_in) || (C != DNA_in) || (G != DNA_in) || (T != DNA_in));
            all_genes_qty <= all_genes_qty_nxt;
            valid_out <= (all_genes_qty_nxt == SYMBOL_QTY) && valid_enable;
        end
    end

    always @(all_genes_qty) begin
    	if(all_genes_qty < SYMBOL_QTY + 20)
           all_genes_qty_nxt = all_genes_qty+1;
        else
            all_genes_qty_nxt = all_genes_qty;
        if(0 == all_genes_qty) valid_enable = 'b0;
        else if(1 == all_genes_qty)  valid_enable = 'b1;
        else if(all_genes_qty > SYMBOL_QTY) valid_enable = 'b0;
    end
    
endmodule


