`timescale 1ns / 1ps
`include "bram_interface.vh"

module ext_mem_pretender
#(parameter ART_DELAY = 3,
  parameter MEMORY_WIDTH = 12,
  parameter DATA_WIDTH = 2)
(
	input wire clock,
	input wire reset,
	bram_DNA_if mem_if
);

localparam ADDR = 'b00;
localparam WAIT = 'b01;
localparam OUT_RELEASE  = 'b10;

reg [DATA_WIDTH-1:0] mem_out, out_release;

reg [MEMORY_WIDTH-1:0] addr_in, addr_in_nxt, addr_out, addr_out_nxt;
reg we_in, we_in_nxt, valid, valid_nxt, ready, ready_nxt;
reg [1:0] in_in, in_in_nxt;
reg[1:0] state, state_nxt;
reg [2:0] wait_ctr, wait_ctr_nxt;

always_ff @(posedge clock or negedge reset) 
begin
	if(reset) begin
		state <= ADDR;
		addr_in <= '{default:1};
		wait_ctr <= 'b0;
		valid <= 'b0;
		//ready <= 'b0;
	end else begin
		addr_in <= addr_in_nxt;
		we_in <= we_in_nxt;
		in_in <= in_in_nxt;
		wait_ctr <= wait_ctr_nxt;
		valid <= valid_nxt;
		addr_out <= addr_out_nxt;
		state <= state_nxt;
	end
end

always_comb
begin
	case (state)
		ADDR: begin
			out_release = mem_out;
			valid_nxt = (addr_in == mem_if.addr) ? (mem_if.re ? valid : 'b0) : 'b0;
			if(mem_if.re || mem_if.we) begin
				addr_in_nxt = mem_if.addr;
				in_in_nxt = mem_if.in;
				we_in_nxt = mem_if.we;
				state_nxt = WAIT;
				wait_ctr_nxt = 'b1;
				ready = 0;
			end else begin
				state_nxt = ADDR;
				ready = 1;
			end
		end

		WAIT: begin
			if(wait_ctr >= (ART_DELAY-2)) begin
				state_nxt = OUT_RELEASE;
			end else begin
			    state_nxt = WAIT;
			    wait_ctr_nxt = wait_ctr+1;
		    end
		end

		OUT_RELEASE: begin
			state_nxt = ADDR;
			ready = 1;
			addr_out_nxt = addr_in;
			if(!we_in)
				valid_nxt = 'b1;
		end
	
		default : /* default */;
	endcase

end

assign mem_if.out = out_release;
assign mem_if.valid = valid;
assign mem_if.ready = ready;
assign mem_if.out_addr = addr_out;

BRAM_DNA DNA_mem_v2 (
  .clka(clock),    // input wire clka
  .wea(we_in),      // input wire [0 : 0] wea
  .addra(addr_in),  // input wire [16 : 0] addra
  .dina(in_in),    // input wire [1 : 0] dina
  .douta(mem_out)  // output wire [1 : 0] douta
);

endmodule
