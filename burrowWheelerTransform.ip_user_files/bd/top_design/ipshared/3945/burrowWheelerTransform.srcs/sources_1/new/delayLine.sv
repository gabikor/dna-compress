`timescale 1ns / 1ps

module delayLine
	#(parameter WIDTH = 16,
				DELAY_CYCLES = 2
	)
	(
    input clk,
    input rst,
    input [WIDTH-1:0] din,
    output reg [WIDTH-1:0] dout
    );
	
	reg [WIDTH-1:0] mem [DELAY_CYCLES];

	assign dout = mem[DELAY_CYCLES-1];

	genvar i;

	for(i = 1; i < DELAY_CYCLES; i+=1)
	begin
		always @(posedge clk)
		begin
			if(rst)
				mem[i] <= 'b0;
			else
				mem[i] <= mem[i-1];
		end
    end

	always @(posedge clk)
		if(rst)
			mem[0] <= 'b0;
		else begin
			mem[0] <= din;
		end

endmodule